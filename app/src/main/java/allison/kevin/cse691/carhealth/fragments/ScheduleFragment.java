package allison.kevin.cse691.carhealth.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import allison.kevin.cse691.car.EdmundsApi;
import allison.kevin.cse691.car.holders.MaintenanceHolder;
import allison.kevin.cse691.car.models.Maintenance;
import allison.kevin.cse691.car.utilities.MaintInfo;
import allison.kevin.cse691.carhealth.R;
import allison.kevin.cse691.carhealth.adapters.ScheduleRecyclerAdapter;
import allison.kevin.cse691.carhealth.database.AndroidCache;
import allison.kevin.cse691.carhealth.database.MaintenanceContract;
import allison.kevin.cse691.carhealth.fragments.steps.StepperFragment;
import allison.kevin.cse691.carhealth.models.MaintenanceInterval;
import allison.kevin.cse691.carhealth.views.SimpleDividerItemDecoration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ScheduleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScheduleFragment extends StepperFragment {

    private static final int MILEAGE_DISPLAY = 1;
    private static final int TIME_DISPLAY = 2;
    private static final int ONCE_DISPLAY = 4;
    private static final int DONE_DISPLAY = 8;

    private ScheduleRecyclerAdapter adapter;

    private AndroidCache cache;
    private int modelYearId;
    private String engine, trans;

    public ScheduleFragment() {
        // Required empty public constructor
    }

    @Override
    protected boolean validateResult() {
        return true;
    }

    @Override
    protected Bundle buildResult() {
        Bundle icicle = new Bundle();
        icicle.putStringArrayList("items", adapter.getSelectedItems());
        return icicle;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CarFragment.
     */
    public static ScheduleFragment newInstance(int id, String engine, String trans) {
        ScheduleFragment fragment = new ScheduleFragment();
        Bundle args = new Bundle();
        args.putInt("modelYearId", id);
        args.putString("engine", engine);
        args.putString("trans", trans);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            modelYearId = getArguments().getInt("modelYearId", 0);
            engine = getArguments().getString("engine", "");
            trans = getArguments().getString("trans", "");
        }

        cache = (AndroidCache)EdmundsApi.getCache();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Create necessary adapters
        Log.d("CarHealth", "getStep(): " + (getStep() == -1));
        List<MaintenanceInterval> intervalList = new ArrayList<>();
        adapter = new ScheduleRecyclerAdapter(getActivity(), intervalList, getStep() != -1);

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_maintenance, container, false);
        final RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.maintenance_list);
        final View progress = view.findViewById(R.id.progress);
        final TextView progressText = (TextView)view.findViewById(R.id.progress_text);
        final Button filterTime = (Button)view.findViewById(R.id.filter_time);
        final Button filterMileage = (Button)view.findViewById(R.id.filter_mileage);
        final Button filterOnce = (Button)view.findViewById(R.id.filter_once);
        final Button filterDone = (Button)view.findViewById(R.id.filter_done);

        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        // Load data, check cache first
        if (!cache.checkMaintenanceCache(modelYearId)) {
            // We need to reach and and download the maintenance information

            // Display loading screen (aka progress overlay)
            progressText.setText(R.string.caching);
            progress.setVisibility(View.VISIBLE);

            Callback<MaintenanceHolder> callback = new Callback<MaintenanceHolder>() {
                @Override
                public void onResponse(Call<MaintenanceHolder> call, Response<MaintenanceHolder> response) {
                    MaintenanceHolder mHolder = response.body();
                    cache.cacheMaintenance(modelYearId, mHolder.actionHolder);

                    loadCachedMaintenance(null, null, MILEAGE_DISPLAY);

                    // Hide progress overlay
                    progress.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<MaintenanceHolder> call, Throwable t) {
                    Log.e("MFrag", "Failed to get maintenance: " + t.getMessage());
                }
            };

            EdmundsApi.getMaintenanceAsync(EdmundsApi.getApi(), modelYearId, callback);
        } else {
            loadCachedMaintenance(null, null, MILEAGE_DISPLAY);
        }

        filterTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get data from cache, then set the adapter
                loadCachedMaintenance(
                        new String[] {MaintenanceContract.MaintenanceEntry.COLUMN_FREQ },
                        new String[] { "4" },
                        TIME_DISPLAY
                );
            }
        });

        filterMileage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get data from cache, then set the adapter
                loadCachedMaintenance(
                        new String[] {MaintenanceContract.MaintenanceEntry.COLUMN_FREQ },
                        new String[] { "4" },
                        MILEAGE_DISPLAY
                );
            }
        });

        filterOnce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadCachedMaintenance(
                        new String[]{ MaintenanceContract.MaintenanceEntry.COLUMN_FREQ },
                        new String[]{ "3" },
                        ONCE_DISPLAY
                );
            }
        });

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        adapter.onSaveInstanceState(icicle);
    }

    @Override
    public void onActivityCreated(Bundle icicle) {
        super.onActivityCreated(icicle);
        adapter.onRestoreInstanceState(icicle);
    }

    // Create the display for showing maintenance based on time
    private List<MaintenanceInterval> buildTimeDisplay(MaintInfo mInfo) {
        List<MaintenanceInterval> intervalList = new ArrayList<>();

        for (Integer month : mInfo.months.keySet()) {
            // Only show 5 years of maintenance
            if ((month / 12) > 5) {
                continue;
            }

            String unit = (month > 11) ? ((month > 23) ? "Years" : "Year")
                    : (month > 1) ? "Month" : "Months";

            int val = (month > 11) ? month / 12 : month;
            String fmtString = String.format(Locale.ENGLISH, "Every %d %s", val, unit);
            MaintenanceInterval interval =
                    new MaintenanceInterval(fmtString, mInfo.months.get(month));

            intervalList.add(interval);
        }

        return intervalList;
    }

    // Create the display for showing maintenance based on mileage
    private List<MaintenanceInterval> buildMileageDisplay(MaintInfo mInfo) {
        List<MaintenanceInterval> intervalList = new ArrayList<>();
        for (Integer mileage : mInfo.mileage.keySet()) {
            String fmtString = String.format(Locale.ENGLISH, "%d Miles", mileage);
            MaintenanceInterval interval =
                    new MaintenanceInterval(fmtString, mInfo.mileage.get(mileage));

            intervalList.add(interval);
        }

        return intervalList;
    }

    private void loadCachedMaintenance(String[] filterColumns, String[] filterValues, int display) {

        // Get data from cache, then set the adapter
        List<Maintenance> mList = cache.getMaintenance(modelYearId, filterColumns, filterValues);
        MaintInfo mInfo = EdmundsApi.sortMaintenance(mList, engine, trans);

        List<MaintenanceInterval> intervalList = new ArrayList<>();
        switch (display) {
            case MILEAGE_DISPLAY:
                intervalList.addAll(buildMileageDisplay(mInfo));
                break;
            case TIME_DISPLAY:
                intervalList.addAll(buildTimeDisplay(mInfo));
                break;
            case ONCE_DISPLAY:
            case DONE_DISPLAY:
            default:
                intervalList.addAll(buildTimeDisplay(mInfo));
                intervalList.addAll(buildMileageDisplay(mInfo));
                break;
        }

        adapter.resetItems(intervalList);
    }
}
