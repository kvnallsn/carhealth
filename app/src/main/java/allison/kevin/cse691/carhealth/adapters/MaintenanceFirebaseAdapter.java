package allison.kevin.cse691.carhealth.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import allison.kevin.cse691.carhealth.R;
import allison.kevin.cse691.carhealth.models.MaintenanceItem;
import allison.kevin.cse691.carhealth.viewholders.MyMaintenanceViewHolder;

/**
 * Created by kevin on 4/14/16.
 *
 * Custom FirebaseArray/Adapter class because the original is too restrictive
 */
public class MaintenanceFirebaseAdapter extends RecyclerView.Adapter<MyMaintenanceViewHolder> implements ChildEventListener {

    private Query mQuery;
    private Map<String, String> filters;
    private List<DataSnapshot> mSnapshots;

    public MaintenanceFirebaseAdapter(Query query, Map<String, String> filters) {
        this.mQuery = query;
        this.mSnapshots = new ArrayList<>();
        this.filters = filters;
        this.mQuery.addChildEventListener(this);
    }

    private int getIndexForKey(String key) throws Exception {

        if (key == null) {
            throw new Exception("MaintenanceFirebaseAdapter.getIndexForKey: Key is null");
        }

        int index = 0;
        for (DataSnapshot snapshot : mSnapshots) {
            if (snapshot.getKey().equals(key)) {
                return index;
            }

            ++index;
        }

        throw new Exception("Error Getting Index (MaintenanceFirebaseAdapter: " + key + ")");
    }

    public int sortByTimestampDesc(DataSnapshot dataSnapshot) {
        int insert = 0;
        MaintenanceItem newItem = dataSnapshot.getValue(MaintenanceItem.class);
        for (DataSnapshot snapshot : mSnapshots) {
            MaintenanceItem item = snapshot.getValue(MaintenanceItem.class);

            // Regular timestamps
            if (newItem.getDate() <= item.getDate()) {
                ++insert;
                continue;
            }

            break;
        }

        return insert;
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String previousChildKey) {
        int index = sortByTimestampDesc(dataSnapshot);
        mSnapshots.add(index, dataSnapshot);
        notifyItemInserted(index);
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String previousChildKey) {
        try {
            int index = getIndexForKey(dataSnapshot.getKey());
            int newIndex = sortByTimestampDesc(dataSnapshot);

            if (index != newIndex) {
                mSnapshots.remove(index);
                mSnapshots.add(newIndex, dataSnapshot);
                notifyItemMoved(index, newIndex);           // Let the adapter know the item moved
                notifyItemChanged(newIndex);                // Let the adapter know the item changed
            } else {
                mSnapshots.set(index, dataSnapshot);
                notifyItemChanged(index);
            }
        } catch (Exception e ) {
            Log.e("CarHealth", "Error: " + e.getMessage());
        }
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
        try {
            int index = getIndexForKey(dataSnapshot.getKey());
            mSnapshots.remove(index);
            notifyItemRemoved(index);
        } catch (Exception e ) {
            Log.e("CarHealth", "Error: " + e.getMessage());
        }
    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String previousChildKey) {
        try {
            int oldIndex = getIndexForKey(dataSnapshot.getKey());
            mSnapshots.remove(oldIndex);
            int newIndex = previousChildKey == null ? 0 : (getIndexForKey(previousChildKey) + 1);
            mSnapshots.add(newIndex, dataSnapshot);
            notifyItemMoved(oldIndex, newIndex);
        } catch (Exception e ) {
            Log.e("CarHealth", "Error: " + e.getMessage());
        }
    }

    @Override
    public void onCancelled(FirebaseError firebaseError) {
        Log.d("CarHealth", "Action Cancelled: " + firebaseError.getMessage());
    }

    public void cleanup() {
        mQuery.removeEventListener(this);
    }

    public void filter(String query) {

    }

    @Override
    public MyMaintenanceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.maintenance_item, parent, false);
        return new MyMaintenanceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyMaintenanceViewHolder holder, int position) {
        MaintenanceItem item = mSnapshots.get(position).getValue(MaintenanceItem.class);
        holder.setData(item);
    }

    @Override
    public int getItemCount() {
        return mSnapshots.size();
    }
}
