package allison.kevin.cse691.carhealth.adapters;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.lang.ref.WeakReference;

import allison.kevin.cse691.carhealth.R;
import allison.kevin.cse691.carhealth.util.FuelUtil;
import allison.kevin.cse691.gas.models.Station;

/**
 * Created by kevin on 4/3/16.
 *
 * Adapter to show a custom view when a marker is clicked
 */
public class FuelMarkerAdapter implements GoogleMap.InfoWindowAdapter {

    private WeakReference<Activity> activity;

    public FuelMarkerAdapter(WeakReference<Activity> activity) {
        this.activity = activity;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;

    }

    @Override
    public View getInfoContents(Marker marker) {
        View v = activity.get().getLayoutInflater().inflate(R.layout.fuel_marker, null);

        TextView name = (TextView)v.findViewById(R.id.station_name);
        TextView dist = (TextView)v.findViewById(R.id.station_distance);
        TextView reg = (TextView)v.findViewById(R.id.station_reg);
        TextView mid = (TextView)v.findViewById(R.id.station_mid);
        TextView pre = (TextView)v.findViewById(R.id.station_pre);
        TextView diesel = (TextView)v.findViewById(R.id.station_diesel);

        FuelUtil fuelUtil = FuelUtil.get();
        Station s = fuelUtil.getStation(Integer.parseInt(marker.getSnippet()));

        name.setText(s.station);
        dist.setText(s.distance);
        reg.setText(s.regular());
        mid.setText(s.midgrade());
        pre.setText(s.premium());
        diesel.setText(s.diesel());

        return v;
    }
}
