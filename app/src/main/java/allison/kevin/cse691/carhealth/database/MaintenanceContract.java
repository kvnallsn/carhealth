package allison.kevin.cse691.carhealth.database;

import android.provider.BaseColumns;

/**
 * Created by kevin on 3/13/16.
 *
 * SQL Schema for the Garage
 */
public final class MaintenanceContract {
    // Prevent from accidentally instantiating the contact class
    private MaintenanceContract() { }

    public static abstract class MaintenanceCacheEntry implements BaseColumns {
        public static final String TABLE_NAME = "maintenance_cache";
        public static final String COLUMN_MODELYEARID = "modelyearid";
    }

    public static abstract class MaintenanceEntry implements BaseColumns {
        public static final String TABLE_NAME = "maintenance";
        public static final String COLUMN_EDMUNDS_ID = "edmundsid";
        public static final String COLUMN_LINKED_UUID = "linkedUuid";
        public static final String COLUMN_MODELYEARID = "modelyearid";
        public static final String COLUMN_ENGINE = "engine";
        public static final String COLUMN_TRANSMISSION = "transmission";
        public static final String COLUMN_MILEAGE = "mileage";
        public static final String COLUMN_MONTH = "month";
        public static final String COLUMN_FREQ = "frequency";
        public static final String COLUMN_ACTION = "action";
        public static final String COLUMN_ITEM = "item";
        public static final String COLUMN_DESC = "description";
        public static final String COLUMN_DRIVE = "drive";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ", ";
    public static final String SQL_CREATE_MAINTENANCE =
            "CREATE TABLE IF NOT EXISTS " + MaintenanceEntry.TABLE_NAME + " (" +
                    MaintenanceEntry._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
                    MaintenanceEntry.COLUMN_EDMUNDS_ID + INTEGER_TYPE + COMMA_SEP +
                    MaintenanceEntry.COLUMN_LINKED_UUID + TEXT_TYPE + COMMA_SEP +
                    MaintenanceEntry.COLUMN_MODELYEARID + INTEGER_TYPE + COMMA_SEP +
                    MaintenanceEntry.COLUMN_ENGINE + TEXT_TYPE + COMMA_SEP +
                    MaintenanceEntry.COLUMN_TRANSMISSION + TEXT_TYPE + COMMA_SEP +
                    MaintenanceEntry.COLUMN_MILEAGE + INTEGER_TYPE + COMMA_SEP +
                    MaintenanceEntry.COLUMN_MONTH + INTEGER_TYPE + COMMA_SEP +
                    MaintenanceEntry.COLUMN_FREQ + INTEGER_TYPE + COMMA_SEP +
                    MaintenanceEntry.COLUMN_ACTION + TEXT_TYPE + COMMA_SEP +
                    MaintenanceEntry.COLUMN_ITEM + TEXT_TYPE + COMMA_SEP +
                    MaintenanceEntry.COLUMN_DESC + TEXT_TYPE + COMMA_SEP +
                    MaintenanceEntry.COLUMN_DRIVE + TEXT_TYPE +
            ")";

    public static final String SQL_CREATE_MAINTENANCE_CACHE =
            "CREATE TABLE IF NOT EXISTS " + MaintenanceCacheEntry.TABLE_NAME + " (" +
                    MaintenanceCacheEntry._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
                    MaintenanceCacheEntry.COLUMN_MODELYEARID + INTEGER_TYPE +
            ")";

    public static final String SQL_DROP_MAINTENANCE =
            "DROP TABLE IF EXISTS " + MaintenanceEntry.TABLE_NAME;

    public static final String SQL_DROP_MAINTENANCE_CACHE =
            "DROP TABLE IF EXISTS " + MaintenanceEntry.TABLE_NAME;
}
