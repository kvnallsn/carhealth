package allison.kevin.cse691.carhealth.viewholders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import allison.kevin.cse691.car.models.Car;
import allison.kevin.cse691.carhealth.CarDetailActivity;
import allison.kevin.cse691.carhealth.R;
import allison.kevin.cse691.carhealth.callbacks.GarageCallback;

/**
 * Created by kevin on 4/10/16.
 *
 * Class to represent a garage item in a recyclerview
 */
public class GarageViewHolder extends RecyclerView.ViewHolder {

    public View parentView;
    private TextView txtCircle;
    private TextView txtName;

    private int modelYearId;
    private int carId;
    private String engine;
    private String trans;

    public GarageViewHolder(View view) {
        super(view);

        this.parentView = view;
        this.parentView.setClickable(true);
        txtCircle = (TextView)view.findViewById(R.id.garage_item_circle);
        txtName = (TextView)view.findViewById(R.id.garage_item_name);
    }

    public void setData(final Car car, final GarageCallback callback) {
        txtCircle.setText(car.make.substring(0,1).toUpperCase());
        txtName.setText(car.toString());

        modelYearId = car.modelYearId;
        carId = car.id;
        engine = car.engine;
        trans = car.transmission;

        parentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent detailIntent = new Intent(context, CarDetailActivity.class);
                detailIntent.putExtra("carId", carId);
                detailIntent.putExtra("modelYearId", modelYearId);
                detailIntent.putExtra("engine", engine);
                detailIntent.putExtra("trans", trans);
                detailIntent.putExtra("uuid", car.getUuid());
                detailIntent.putExtra("car", car.toString());
                context.startActivity(detailIntent);
            }
        });

        parentView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                callback.enterActionMode(car, v);
                v.setSelected(true);
                return true;
            }
        });
    }
}