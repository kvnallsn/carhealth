package allison.kevin.cse691.carhealth.models;

import android.location.Location;

/**
 * Created by kevin on 4/3/16.
 *
 * Helper class to request fuel stations in a given area
 */
public class FuelRefreshRequest {
    public final Location location;
    public final int distance;
    public final String type;
    public final String sortBy;

    public FuelRefreshRequest(Location location, int distance, String type, String sortBy) {
        this.location = location;
        this.distance = distance;
        this.type = type;
        this.sortBy = sortBy;
    }
}