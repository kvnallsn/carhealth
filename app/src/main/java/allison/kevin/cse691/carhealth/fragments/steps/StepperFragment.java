package allison.kevin.cse691.carhealth.fragments.steps;


import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class StepperFragment extends Fragment {

    private int step;

    public StepperFragment() {
        // Required empty public constructor
        step = -1;
    }

    public void setStep(int pos) {
        step = pos;
    }

    public int getStep() {
        return step;
    }

    public final boolean validateInput() {
        return validateResult();
    }

    public final Bundle getResults() {
        Bundle icicle = buildResult();
        icicle.putInt("step", step);
        return icicle;
    }

    /**
     * Checks to make sure all necessary fields are completed.  If this returns false,
     * the stepper will not move on to the next step/fragment.
     *
     * @return True if all fields are completed and valid, false otherwise
     */
    protected abstract boolean validateResult();

    /**
     * Method to construct information obtained from using the stepper
     * Each fragment should override this method.  This method is called internally to
     * build the final result for a fragment
     *
     * @return The results/information obtained from this fragment in the stepper
     */
    protected abstract Bundle buildResult();
}
