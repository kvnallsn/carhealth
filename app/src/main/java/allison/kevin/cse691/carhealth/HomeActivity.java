package allison.kevin.cse691.carhealth;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import allison.kevin.cse691.car.EdmundsApi;
import allison.kevin.cse691.carhealth.callbacks.FragmentCallback;
import allison.kevin.cse691.carhealth.database.AndroidCache;
import allison.kevin.cse691.carhealth.fragments.GarageFragment;
import allison.kevin.cse691.carhealth.fragments.GasListFragment;
import allison.kevin.cse691.carhealth.util.CarFirebase;
import allison.kevin.cse691.carhealth.util.LocationUtil;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FragmentCallback {

    enum FRAGMENTS {
        GARAGE,
        GAS,
        SETTINGS
    }

    private FRAGMENTS activeFragment;
    private Fragment mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Set EdmundsApi cache type (default is NoCache)
        EdmundsApi.setCache(new AndroidCache(this));

        LocationUtil.init(new WeakReference<Activity>(this));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View navHeader = navigationView.getHeaderView(0);
        final TextView userView = (TextView)navHeader.findViewById(R.id.nav_drawer_user);
        final TextView emailView = (TextView)navHeader.findViewById(R.id.nav_drawer_email);
        final ImageView profileView = (ImageView)navHeader.findViewById(R.id.nav_drawer_image);
        userView.setText(CarFirebase.getUserName());
        emailView.setText(CarFirebase.getUserEmail());

        (new Thread(new Runnable() {
            @Override
            public void run() {
                String url = CarFirebase.getUserImageUrl();

                if (url.equals("")) {
                    return;
                }

                try {
                    HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.connect();

                    InputStream inputStream = conn.getInputStream();
                    final Drawable img = Drawable.createFromStream(inputStream, "");

                    conn.disconnect();
                    profileView.post(new Runnable() {
                        @Override
                        public void run() {
                            // On UI Thread
                            profileView.setImageDrawable(img);
                        }
                    });
                } catch (Exception e) {
                    Log.e("CarHealth", "Failed to download user profile image: " + e.getMessage());
                    Log.e("CarHealth", "URL of profile image: " + url);
                }
            }
        })).start();

        if (savedInstanceState == null) {
            // No fragment loaded
            mContent = GarageFragment.newInstance();
            activeFragment = FRAGMENTS.GARAGE;
        } else {
            mContent = getSupportFragmentManager().getFragment(savedInstanceState, "fragment");
            activeFragment = (FRAGMENTS)savedInstanceState.getSerializable("activeFragEnum");
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.home_container, mContent)
                .commit();
    }

    @Override
    public void cancelActivity() {
        finish();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the fragments instance
        getSupportFragmentManager().putFragment(outState, "fragment", mContent);
        outState.putSerializable("activeFragEnum", activeFragment);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_garage) {
            if (activeFragment != FRAGMENTS.GARAGE) {
                mContent = GarageFragment.newInstance();
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.home_container, mContent)
                        .commit();

                activeFragment = FRAGMENTS.GARAGE;
            }
        } else if (id == R.id.nav_gas) {
            if (activeFragment != FRAGMENTS.GAS) {
                mContent = GasListFragment.newInstance();
                Bundle args = new Bundle();
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.home_container, mContent)
                        .commit();

                activeFragment = FRAGMENTS.GAS;
            }
        } else if (id == R.id.nav_settings) {
            if (activeFragment != FRAGMENTS.SETTINGS) {
                activeFragment = FRAGMENTS.SETTINGS;
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_garage, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_garage_logout:
                CarFirebase.logout();
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }
}
