package allison.kevin.cse691.carhealth.fragments.steps;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import java.util.GregorianCalendar;

import allison.kevin.cse691.carhealth.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarFragment extends StepperFragment {

    private long date;

    public CalendarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);
        CalendarView calendarView = (CalendarView)view.findViewById(R.id.calendar);
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                GregorianCalendar calendar = new GregorianCalendar(year, month, dayOfMonth);
                date = calendar.getTimeInMillis();
            }
        });
        return view;
    }

    @Override
    protected boolean validateResult() {
        return (date > 0);
    }

    @Override
    public Bundle buildResult() {
        Bundle icicle = new Bundle();
        icicle.putLong("date", date);
        return icicle;
    }
}
