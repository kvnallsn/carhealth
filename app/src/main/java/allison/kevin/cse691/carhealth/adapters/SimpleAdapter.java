package allison.kevin.cse691.carhealth.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import allison.kevin.cse691.carhealth.R;

/**
 * Created by kevin on 3/19/16.
 *
 * Adapter to display car data (aka manufacturers)
 */
public class SimpleAdapter<T> extends RecyclerView.Adapter<SimpleAdapter.SimpleViewHolder> {

    public interface SimpleAdapterCallback<T> {
        void onClick(T data);
    }

    /**
     * ViewHolder inner class
     */
    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        private View view;
        private TextView name;

        public SimpleViewHolder(View itemView) {
            super(itemView);

            view = itemView;
            name = (TextView)itemView.findViewById(R.id.add_car_text);
        }

        public void bind(String make) {
            name.setText(make);
        }
    }

    private List<T> data;
    private SimpleAdapterCallback callback;

    public SimpleAdapter(SimpleAdapterCallback callback) {
        data = new ArrayList<>();
        this.callback = callback;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_add_car_item, parent, false);

        return new SimpleViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        holder.bind(data.get(position).toString());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClick(data.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<T> data) {
        this.data = data;
        notifyItemRangeInserted(0, data.size());
    }
}
