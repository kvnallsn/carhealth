package allison.kevin.cse691.carhealth.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.client.Query;

import allison.kevin.cse691.carhealth.AddMaintenanceStepper;
import allison.kevin.cse691.carhealth.HomeActivity;
import allison.kevin.cse691.carhealth.R;
import allison.kevin.cse691.carhealth.adapters.MaintenanceFirebaseAdapter;
import allison.kevin.cse691.carhealth.util.CarFirebase;
import allison.kevin.cse691.carhealth.views.SimpleDividerItemDecoration;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyMaintenaceFragment extends Fragment {

    private static final int GET_FILTERS = 0;
    private MaintenanceFirebaseAdapter maintenanceFirebaseAdapter;

    public static MyMaintenaceFragment newInstance(String uuid,
                                                   int modelYearId, String engine, String trans) {
        MyMaintenaceFragment fragment = new MyMaintenaceFragment();
        Bundle args = new Bundle();
        args.putString("uuid", uuid);
        args.putInt("modelYearId", modelYearId);
        args.putString("engine", engine);
        args.putString("trans", trans);
        fragment.setArguments(args);
        return fragment;
    }

    public MyMaintenaceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle args = getArguments();
        final String uuid = (args == null) ? "" : getArguments().getString("uuid");
        final int modelYearId = (args == null) ? 0: args.getInt("modelYearId");
        final String engine = (args == null) ? "": args.getString("engine");
        final String trans = (args == null) ? "": args.getString("trans");

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_maintenace, container, false);
        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.maint_recyclerview);

        Query ref= null;
        try {
            ref = CarFirebase.getCurrentUserMaintRef()
                .orderByChild("carUuid")
                .equalTo(uuid);
        } catch (Exception e) {
            Log.d("CarHealth", "MyMaintenance: User not logged in!");
            getActivity().finish();
        }

        maintenanceFirebaseAdapter = new MaintenanceFirebaseAdapter(ref, null);

        recyclerView.setAdapter(maintenanceFirebaseAdapter);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        FloatingActionButton fab = (FloatingActionButton)view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyMaintenaceFragment.this.getActivity(),
                        AddMaintenanceStepper.class);
                intent.putExtra("uuid", uuid);
                intent.putExtra("modelYearId", modelYearId);
                intent.putExtra("engine", engine);
                intent.putExtra("trans", trans);
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_my_maintenance, menu);

        MenuItem searchViewMenuItem = menu.findItem(R.id.menu_maint_search);
        final SearchView searchView = (SearchView)searchViewMenuItem.getActionView();

        if (searchView != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.d("CarHealth", query);
                    maintenanceFirebaseAdapter.filter(query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });
        }

        MenuItemCompat.setOnActionExpandListener(searchViewMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                return true;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_maint_filter:
                Intent intent = new Intent(getContext(), HomeActivity.class);
                startActivityForResult(intent, GET_FILTERS);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public void onDestroy() {
        maintenanceFirebaseAdapter.cleanup();
        super.onDestroy();
    }
}
