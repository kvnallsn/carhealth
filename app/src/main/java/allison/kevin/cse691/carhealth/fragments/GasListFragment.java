package allison.kevin.cse691.carhealth.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import allison.kevin.cse691.carhealth.FuelMapActivity;
import allison.kevin.cse691.carhealth.R;
import allison.kevin.cse691.carhealth.adapters.FuelAdapter;
import allison.kevin.cse691.carhealth.callbacks.ProgressCallback;
import allison.kevin.cse691.carhealth.models.FuelRefreshRequest;
import allison.kevin.cse691.carhealth.util.LocationUtil;
import allison.kevin.cse691.gas.GasApi;

/**
 * A simple {@link Fragment} subclass.
 */
public class GasListFragment extends Fragment {

    // Local Variables
    private FuelAdapter fuelAdapter;
    private LocationUtil locationUtil;
    private ProgressCallback progressCallback;

    // UI Fields
    private TextView progressText;
    private View progressView;
    private View emptyView;
    private View mainView;

    public static GasListFragment newInstance() {
        GasListFragment frag = new GasListFragment();
        Bundle icicle = new Bundle();
        frag.setArguments(icicle);
        return  frag;
    }

    public GasListFragment() {
        GasApi.setApiKey("jya3fj3cxi");
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        locationUtil = LocationUtil.get();
        locationUtil.initAndroidService();
        //locationUtil.useMockData(true);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle icicle) {
        super.onActivityCreated(icicle);
        if (icicle != null) {
            LocationUtil.get().setLastLocation((Location)icicle.getParcelable("location"));
        }

        Location newLocation = locationUtil.getLastKnownLocationAndroid();
        if (newLocation != null) {
            fuelAdapter.refresh(new FuelRefreshRequest(newLocation, 5, "reg", "distance"),
                    progressCallback, false);
        }

        getActivity().setTitle("Nearby Fuel");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainView =  inflater.inflate(R.layout.fragment_gas_list, container, false);

        final RecyclerView recyclerView = (RecyclerView)mainView.findViewById(R.id.gas_recyclerView);
        progressView = mainView.findViewById(R.id.progress);
        progressText = (TextView)mainView.findViewById(R.id.progress_text);
        emptyView = mainView.findViewById(R.id.empty);

        // Ensure progress view is visible and empty view is gone
        progressView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);

        progressCallback = new ProgressCallback() {
            @Override
            public void setText(String text) {
                progressText.setText(text);
            }

            @Override
            public void setText(int resId) {
                progressText.setText(resId);
            }

            @Override
            public void onSuccess() {
                progressView.setVisibility(View.GONE);
                if (fuelAdapter.getItemCount() == 0) {
                    emptyView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure() {
                progressView.setVisibility(View.GONE);
            }
        };

        fuelAdapter = new FuelAdapter(new WeakReference<Activity>(getActivity()));

        recyclerView.setAdapter(fuelAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return mainView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("location", LocationUtil.get().getLastLocation());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fuel_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_fuel_map:
                Intent intent = new Intent(getContext(), FuelMapActivity.class);
                startActivity(intent);
                break;
            case R.id.menu_fuel_refresh:
                Location newLocation = LocationUtil.get().getLastKnownLocationAndroid();
                progressText.setText(R.string.retrieve_gas_list);
                progressView.setVisibility(View.VISIBLE);
                fuelAdapter.refresh(new FuelRefreshRequest(newLocation, 5, "reg", "distance"),
                        progressCallback, true);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] results) {

        switch (requestCode) {
            case LocationUtil.REQUEST_FINE_LOCATION_PERMISSION:
                if (permissions.length > 0 &&
                        results[0] == PackageManager.PERMISSION_GRANTED) {

                    // Get user's GPS location
                    Location newLocation = locationUtil.getLastKnownLocationAndroid();
                    if (newLocation != null) {
                        fuelAdapter.refresh(new FuelRefreshRequest(newLocation, 5, "reg", "distance"),
                                progressCallback, false);
                    } else {
                        locationUtil.updateAndroidLocation();
                    }
                }

                break;
            default:
                Log.d("CarHealth", "Unknown Permission Request Code");
                super.onRequestPermissionsResult(requestCode, permissions, results);
                break;
        }
    }
}
