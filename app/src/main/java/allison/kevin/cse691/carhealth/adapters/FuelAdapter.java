package allison.kevin.cse691.carhealth.adapters;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.List;

import allison.kevin.cse691.carhealth.FuelMapActivity;
import allison.kevin.cse691.carhealth.R;
import allison.kevin.cse691.carhealth.callbacks.ProgressCallback;
import allison.kevin.cse691.carhealth.models.FuelRefreshRequest;
import allison.kevin.cse691.carhealth.util.FuelUtil;
import allison.kevin.cse691.carhealth.util.LocationUtil;
import allison.kevin.cse691.gas.GasApi;
import allison.kevin.cse691.gas.holders.GasStationHolder;
import allison.kevin.cse691.gas.models.Station;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kevin on 4/3/16.
 *
 * Adapter to handle fuel (aka gas) stations
 */
public class FuelAdapter extends RecyclerView.Adapter<FuelAdapter.FuelViewHolder> {

    ///////////////////////////////////////
    // ViewHolder
    ///////////////////////////////////////
    class FuelViewHolder extends RecyclerView.ViewHolder {

        private View card;
        private TextView stationName;
        private TextView stationRegularPrice;
        private TextView stationMidPrice;
        private TextView stationPremiumPrice;
        private TextView stationDieselPrice;
        private TextView stationDistance;

        public FuelViewHolder(View itemView) {
            super(itemView);

            card = itemView;
            stationName = (TextView)itemView.findViewById(R.id.station_name);
            stationRegularPrice = (TextView)itemView.findViewById(R.id.station_reg);
            stationMidPrice = (TextView)itemView.findViewById(R.id.station_mid);
            stationPremiumPrice = (TextView)itemView.findViewById(R.id.station_pre);
            stationDieselPrice = (TextView)itemView.findViewById(R.id.station_diesel);
            stationDistance = (TextView)itemView.findViewById(R.id.station_distance);
        }

        public void bind(Station station) {

            stationName.setText(station.station);
            stationRegularPrice.setText(station.regular());
            stationMidPrice.setText(station.midgrade());
            stationPremiumPrice.setText(station.premium());
            stationDieselPrice.setText(station.diesel());
            stationDistance.setText(station.distance);

            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Activity act = FuelAdapter.this.activity.get();
                    Intent mapIntent = new Intent(act.getApplicationContext(), FuelMapActivity.class);
                    mapIntent.putExtra("id", getAdapterPosition());
                    act.startActivity(mapIntent);
                }
            });
        }
    }

    private FuelUtil fuelUtil;
    private WeakReference<Activity> activity;

    public FuelAdapter(WeakReference<Activity> activity) {
        this.fuelUtil = FuelUtil.get();
        this.activity = activity;
    }

    ///////////////////////////////////////
    // Overriden Methods
    ///////////////////////////////////////

    @Override
    public FuelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fuel_station_item, parent, false);

        return new FuelViewHolder(v);
    }

    @Override
    public void onBindViewHolder(FuelViewHolder holder, int position) {
        holder.bind(fuelUtil.getStation(holder.getAdapterPosition()));
    }

    @Override
    public int getItemCount() {
        return fuelUtil.getStationList().size();
    }

    ///////////////////////////////////////
    // Instance Methods
    ///////////////////////////////////////
    public void addStation(Station station) {
        fuelUtil.addStation(station);
        notifyItemInserted(fuelUtil.getStationList().size() - 1);
    }

    public void addStationList(List<Station> stationList) {
        int start = fuelUtil.getStationList().size();
        fuelUtil.addStations(stationList);
        notifyItemRangeInserted(start, stationList.size());
    }

    public void clearStations() {
        int end = fuelUtil.getStationList().size();
        fuelUtil.clear();
        notifyItemRangeRemoved(0, end);
    }

    public void clearAndAddStationList(List<Station> stationList) {
        clearStations();
        addStationList(stationList);
    }

    public void refresh(FuelRefreshRequest request,
                        final ProgressCallback progressCallback, boolean force) {
        Location lastLocation = LocationUtil.get().getLastLocation();

        if (!force && lastLocation != null) {
            // Check to see if the user has moved
            if ((Math.abs(lastLocation.getLatitude() - request.location.getLatitude()) < 0.1) &&
                    (Math.abs(lastLocation.getLongitude() - request.location.getLongitude()) < 0.1)) {

                // No need to update
                progressCallback.onSuccess();
                return;
            }
        }

        clearStations();
        LocationUtil.get().setLastLocation(request.location);
        progressCallback.setText(R.string.retrieve_gas_list);
        GasApi.getNearbyStationsAsync(request.location.getLatitude(), request.location.getLongitude(),
                request.distance, request.type, request.sortBy, new Callback<GasStationHolder>() {

                    @Override
                    public void onResponse(Call<GasStationHolder> call, Response<GasStationHolder> response) {
                        addStationList(response.body().stations);
                        progressCallback.onSuccess();
                    }

                    @Override
                    public void onFailure(Call<GasStationHolder> call, Throwable t) {
                        Log.e("CarHealth", "Failed to retrieve station data: " + t.getMessage());
                        progressCallback.onFailure();
                    }
                });
    }
}
