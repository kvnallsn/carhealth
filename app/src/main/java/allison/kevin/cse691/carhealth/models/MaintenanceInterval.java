package allison.kevin.cse691.carhealth.models;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.List;

import allison.kevin.cse691.car.models.Maintenance;

/**
 * Created by kevin on 3/17/16.
 *
 * Represents and interval item for an expandable recyclerview
 */
public class MaintenanceInterval implements ParentListItem {
    private List<Maintenance> maintenanceList;
    public final String interval;

    public MaintenanceInterval(String interval, List<Maintenance> maintenanceList) {
        this.interval = interval;
        this.maintenanceList = maintenanceList;
    }

    @Override
    public List<Maintenance> getChildItemList() {
        return maintenanceList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
