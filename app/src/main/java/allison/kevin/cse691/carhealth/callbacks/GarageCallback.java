package allison.kevin.cse691.carhealth.callbacks;

import android.view.View;

import allison.kevin.cse691.car.models.Car;

/**
 * Created by kevin on 3/15/16.
 *
 * Interface for callbacks to the GarageFragment
 */
public interface GarageCallback {
    void enterActionMode(Car car, View view);
    void exitActionMode();
    void deleteCar();
}
