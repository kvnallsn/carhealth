package allison.kevin.cse691.carhealth;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import allison.kevin.cse691.carhealth.fragments.steps.StepperFragment;

public abstract class StepperActivity extends AppCompatActivity {

    private TextView stepString;
    private TextView nextBtn;
    private TextView backBtn;

    private int stepCount;
    private int currentStep;
    private Bundle args;
    private List<Class<? extends StepperFragment>> fragmentList;

    private StepperFragment currentFragment;

    // Methods that must be overridden, detail how to use this class plus
    // different interactions available
    protected abstract void onStepperInit(List<Class<? extends StepperFragment>> fragmentList);
    protected abstract void onNextStep(Bundle results);
    protected abstract void onStepperFinish();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stepper);

        // Set up stepper properties
        currentStep = 0;
        fragmentList = new ArrayList<>();
        onStepperInit(fragmentList);
        stepCount = fragmentList.size();

        // Configure UI Elements
        stepString = (TextView)findViewById(R.id.steps_string);
        nextBtn = (TextView)findViewById(R.id.steps_next);
        backBtn = (TextView)findViewById(R.id.steps_back);
        backBtn.setVisibility(View.INVISIBLE);

        stepString.setText(buildStepString());
        if (currentStep == (stepCount - 1)) {
            nextBtn.setText(R.string.finish);
        }

        // Load the fragment at position 0, if it exists
        if (stepCount > 0) {
            loadFragment(0);
        }
    }

    /**
     * Utility function to set arguments passed to the fragments
     *
     * @param args Arguments to pass to the stepper fragments
     */
    public void setFragmentArgs(Bundle args) {
        this.args = args;
    }

    private void loadFragment(int pos) {
        Class<? extends StepperFragment> clazz = fragmentList.get(pos);
        try {
            currentFragment = clazz.newInstance();
            currentFragment.setStep(pos);
            currentFragment.setArguments(args);

            FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.steps_container, currentFragment);

            if (pos != 0) {
                fragmentTransaction.addToBackStack(null);
            }

            fragmentTransaction.commit();

        } catch (Exception e) {
            Log.e("CarHealth", "Could not init class: " + e.getMessage());
        }
    }

    private String buildStepString() {
        return String.format(Locale.getDefault(), "Step %d of %d.", currentStep + 1, stepCount);
    }

    /**
     * On click callback for the next step button
     *
     * @param view  Button that was clicked
     */
    public void nextStep(View view) {
        if (!currentFragment.validateInput()) {
            // Error, do not continue to next step
            Snackbar.make(view, "Must complete all fields", Snackbar.LENGTH_LONG).show();
            return;
        }

        onNextStep(currentFragment.getResults());
        if (++currentStep < stepCount) {
            loadFragment(currentStep);

            if (currentStep == (stepCount - 1)) {
                nextBtn.setText(R.string.finish);
            }
        } else {
            // Finish!
            onStepperFinish();
            finish();
        }

        stepString.setText(buildStepString());
        backBtn.setVisibility(View.VISIBLE);
    }

    /**
     * On click callback for the previous (back) step button
     *
     * @param view  Button that was clicked
     */
    public void previousStep(View view) {
        if (currentStep > 0) {
            getSupportFragmentManager().popBackStack();
            --currentStep;

            if (currentStep == 0) {
                backBtn.setVisibility(View.INVISIBLE);
            }
            nextBtn.setText(R.string.next);
        }

        stepString.setText(buildStepString());

    }
}
