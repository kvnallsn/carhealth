package allison.kevin.cse691.carhealth.fragments.steps;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import allison.kevin.cse691.carhealth.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the FragmentCallback
 * interface
 * to handle interaction events.
 * Use the {@link AddMaintenanceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddMaintenanceFragment extends StepperFragment {

    // Class UI Elements
    private TextView costView;
    private TextView mileageView;
    private TextView descView;
    private Spinner categoryView;

    // Class Global Fields
    private double cost;
    private double mileage;
    private String description;
    private String category;

    private String uuid;
    private String engine;
    private String trans;
    private int modelYearId;

    public AddMaintenanceFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AddMaintenanceFragment.
     */
    public static AddMaintenanceFragment newInstance() {
        AddMaintenanceFragment fragment = new AddMaintenanceFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            uuid = args.getString("uuid");
            engine = args.getString("engine");
            trans = args.getString("trans");
            modelYearId = args.getInt("modelYearId", 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_maintenance, container, false);

        costView = (TextView)view.findViewById(R.id.add_maint_cost);
        mileageView = (TextView)view.findViewById(R.id.add_maint_mileage);
        descView = (TextView)view.findViewById(R.id.add_maint_desc);
        categoryView = (Spinner)view.findViewById(R.id.add_maint_spinner);

        return view;
    }

    @Override
    protected boolean validateResult() {
        // Get cost/mileage and convert to double
        try {
            cost = Double.parseDouble(costView.getText().toString());
            mileage = Double.parseDouble(mileageView.getText().toString());
        } catch (NumberFormatException ex) {
            Log.e("CarHealth", "Error converting strings to doubles: " + ex.getMessage());
            return false;
        }
        category = categoryView.getSelectedItem().toString();
        description = descView.getText().toString();

        return (mileage > 0) &&
                (cost >= 0) &&
                !"".equals(category) &&
                !"".equals(description);
    }

    @Override
    protected Bundle buildResult() {
        Bundle icicle = new Bundle();
        icicle.putString("description", description);
        icicle.putString("category", category);
        icicle.putDouble("cost", cost);
        icicle.putDouble("mileage", mileage);
        return icicle;
    }
}
