package allison.kevin.cse691.carhealth.models;

import java.util.Locale;

/**
 * Created by kevin on 4/26/16.
 */
public class CompletedItem {
    private final String id;

    public CompletedItem(String id) {
        this.id = id;
    }

    public CompletedItem(int edmunds, String carUuid) {
        id = String.format(Locale.ENGLISH, "%d_%s", edmunds, carUuid);
    }

    public String getId() {
        return id;
    }
}
