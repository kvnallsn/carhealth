package allison.kevin.cse691.carhealth.viewholders;

import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

import allison.kevin.cse691.carhealth.R;
import allison.kevin.cse691.carhealth.models.MaintenanceItem;
import allison.kevin.cse691.carhealth.util.DateUtil;

/**
 * Created by kevin on 4/10/16.
 *
 * View holder for a maintenance item
 */
public class MyMaintenanceViewHolder extends RecyclerView.ViewHolder {

    private TextView category;
    private TextView date;
    private TextView action;
    private TextView miles;

    public MyMaintenanceViewHolder(View itemView) {
        super(itemView);

        category = (TextView)itemView.findViewById(R.id.maint_category);
        date = (TextView)itemView.findViewById(R.id.maint_date);
        action = (TextView)itemView.findViewById(R.id.maint_action);
        miles = (TextView)itemView.findViewById(R.id.maint_miles);
    }

    public void setData(MaintenanceItem item) {
        String mileage = String.format(
                Locale.getDefault(), "%.0f miles", item.getMileage()
        );

        category.setText(item.getCategory());
        action.setText(item.getDescription());
        date.setText(DateFormat.format("d MMM yyyy", DateUtil.retrieveTimeStamp(item.getDate())));
        miles.setText(mileage);
    }
}
