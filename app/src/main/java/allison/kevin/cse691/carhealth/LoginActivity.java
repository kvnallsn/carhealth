package allison.kevin.cse691.carhealth;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.ui.auth.core.AuthProviderType;
import com.firebase.ui.auth.core.FirebaseLoginBaseActivity;
import com.firebase.ui.auth.core.FirebaseLoginError;

import allison.kevin.cse691.carhealth.util.CarFirebase;

public class LoginActivity extends FirebaseLoginBaseActivity {

    private Firebase firebase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Log.d("CarHealth", "Connecting to Firebase");
        CarFirebase.init(getApplicationContext().getApplicationContext());
        firebase = CarFirebase.getFirebaseRef();
    }

    @Override
    public void onStart() {
        super.onStart();
        setEnabledAuthProvider(AuthProviderType.GOOGLE);
    }

    @Override
    protected Firebase getFirebaseRef() {
        return firebase;
    }

    public void login_button_onclick(View v) {
        showFirebaseLoginPrompt();
    }

    @Override
    public void onFirebaseLoggedIn(AuthData authData) {
        Intent garageIntent = new Intent(this, HomeActivity.class);
        startActivity(garageIntent);
    }

    @Override
    protected void onFirebaseLoginProviderError(FirebaseLoginError firebaseLoginError) {
        Log.e("CarHealth", "Login Provider Error: " + firebaseLoginError.message);
    }

    @Override
    protected void onFirebaseLoginUserError(FirebaseLoginError firebaseLoginError) {
        Log.e("CarHealth", "Login User Error: " + firebaseLoginError.message);
    }
}
