package allison.kevin.cse691.carhealth.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by kevin on 3/13/16.
 *
 * Helper class to interact with the Database
 * Creates tables, handles upgrades, etc
 */
public class DbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 8;
    public static final String DATABASE_NAME = "CarHealth.db";

    public DbHelper(Context ctx) {
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL(GarageContract.SQL_CREATE_GARAGE);
        db.execSQL(MaintenanceContract.SQL_CREATE_MAINTENANCE);
        db.execSQL(MaintenanceContract.SQL_CREATE_MAINTENANCE_CACHE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL(GarageContract.SQL_DROP_GARAGE);
        db.execSQL(MaintenanceContract.SQL_DROP_MAINTENANCE);
        db.execSQL(MaintenanceContract.SQL_DROP_MAINTENANCE_CACHE);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
