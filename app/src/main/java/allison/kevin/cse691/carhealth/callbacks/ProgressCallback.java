package allison.kevin.cse691.carhealth.callbacks;

/**
 * Created by kevin on 4/3/16.
 *
 * Callback for progress updates
 */
public interface ProgressCallback {
    void setText(String text);
    void setText(int resId);
    void onSuccess();
    void onFailure();
}