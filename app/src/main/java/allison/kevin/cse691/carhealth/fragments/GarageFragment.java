package allison.kevin.cse691.carhealth.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.client.Firebase;

import allison.kevin.cse691.car.EdmundsApi;
import allison.kevin.cse691.car.models.Car;
import allison.kevin.cse691.carhealth.AddCarStepper;
import allison.kevin.cse691.carhealth.R;
import allison.kevin.cse691.carhealth.adapters.GarageFirebaseAdapter;
import allison.kevin.cse691.carhealth.callbacks.FragmentCallback;
import allison.kevin.cse691.carhealth.callbacks.GarageCallback;
import allison.kevin.cse691.carhealth.database.AndroidCache;
import allison.kevin.cse691.carhealth.util.CarFirebase;
import allison.kevin.cse691.carhealth.views.SimpleDividerItemDecoration;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GarageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GarageFragment extends Fragment {

    static final int ADD_CAR_REQUEST = 1;

    private GarageFirebaseAdapter garageAdapter;
    private GarageCallback garageCallback;
    private Firebase firebase;
    private ActionMode mActionMode;

    private FragmentCallback fragmentCallback;

    public GarageFragment() {

        garageCallback = new GarageCallback() {
            private Car car;
            private View view;

            @Override
            public void enterActionMode(Car car, View view) {
                if (mActionMode != null) {
                    return;
                }

                mActionMode = getActivity().startActionMode(mCallback);
                this.car = car;
                this.view = view;
            }

            @Override
            public void deleteCar() {
                try {
                    String uuid = car.getUuid();
                    Firebase carsRef = CarFirebase.getCar(uuid);
                    carsRef.removeValue();

                    // Delete maintenance items from DB
                    AndroidCache cache = (AndroidCache)EdmundsApi.getCache();
                    cache.deleteMaintenance(uuid);
                    view = null;
                } catch (Exception ex) {
                    Log.e("CarHealth", "Error deleting car: " + car.getUuid());
                }
            }

            @Override
            public void exitActionMode() {
                if (view != null) {
                    view.setSelected(false);
                    view = null;
                }
                car = null;
                mActionMode = null;
            }
        };
    }

    private ActionMode.Callback mCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater menuInflater = mode.getMenuInflater();
            menuInflater.inflate(R.menu.action_mode_garage, menu);
            return true;
        }

        // Called each time the action mode is shown
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;   // Return false if nothing to do
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_mode_delete:
                    garageCallback.deleteCar();
                    mode.finish();
                    return true;
                default:
                    break;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            garageCallback.exitActionMode();
        }
    };

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GarageFragment.
     */
    public static GarageFragment newInstance() {
        GarageFragment fragment = new GarageFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentCallback) {
            fragmentCallback = (FragmentCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentCallback");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //if (getArguments() != null) {
        //}

        getActivity().setTitle("My Garage");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_garage, container, false);

        try {
            firebase = CarFirebase.getCurrentUserCarsRef();
        } catch (Exception e) {
            Log.d("CarHealth", "GarageFragment: User is not logged in");
            fragmentCallback.cancelActivity();
            return view;
        }

        garageAdapter = new GarageFirebaseAdapter(Car.class, R.layout.garage_item,
                firebase, getActivity().getApplicationContext(), garageCallback);

        RecyclerView garageList = (RecyclerView)view.findViewById(R.id.garage_list);
        garageList.setLayoutManager(new LinearLayoutManager(getContext()));
        garageList.addItemDecoration(new SimpleDividerItemDecoration(getContext()));
        garageList.setAdapter(garageAdapter);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.garage_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getContext(),
                        AddCarStepper.class), ADD_CAR_REQUEST);
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ADD_CAR_REQUEST:
                garageAdapter.notifyDataSetChanged();
                break;
            default:
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (garageAdapter != null) {
            garageAdapter.cleanup();
        }

        if (firebase != null) {
            firebase.unauth();
        }
    }
}
