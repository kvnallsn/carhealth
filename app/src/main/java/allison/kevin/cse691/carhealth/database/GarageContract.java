package allison.kevin.cse691.carhealth.database;

import android.provider.BaseColumns;

/**
 * Created by kevin on 3/13/16.
 *
 * SQL Schema for the Garage
 */
public final class GarageContract {
    // Prevent from accidentally instantiating the contact class
    public GarageContract() { }

    public static abstract class GarageEntry implements BaseColumns {
        public static final String TABLE_NAME = "garage";
        public static final String COLUMN_MAKE = "make";
        public static final String COLUMN_MODEL = "model";
        public static final String COLUMN_YEAR = "year";
        public static final String COLUMN_STYLE = "style";
        public static final String COLUMN_MODELYEARID = "modelyearid";
        public static final String COLUMN_STYLEID = "styleid";
        public static final String COLUMN_ENGINE = "engine";
        public static final String COLUMN_TRANSMISSION = "transmission";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ", ";
    public static final String SQL_CREATE_GARAGE =
            "CREATE TABLE IF NOT EXISTS " + GarageEntry.TABLE_NAME + " (" +
                    GarageEntry._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
                    GarageEntry.COLUMN_MAKE + TEXT_TYPE + COMMA_SEP +
                    GarageEntry.COLUMN_MODEL + TEXT_TYPE + COMMA_SEP +
                    GarageEntry.COLUMN_YEAR + INTEGER_TYPE + COMMA_SEP +
                    GarageEntry.COLUMN_STYLE + TEXT_TYPE + COMMA_SEP +
                    GarageEntry.COLUMN_MODELYEARID + INTEGER_TYPE + COMMA_SEP +
                    GarageEntry.COLUMN_STYLEID + INTEGER_TYPE + COMMA_SEP +
                    GarageEntry.COLUMN_ENGINE + TEXT_TYPE + COMMA_SEP +
                    GarageEntry.COLUMN_TRANSMISSION + TEXT_TYPE +
            ")";

    public static final String SQL_DROP_GARAGE =
            "DROP TABLE IF EXISTS " + GarageEntry.TABLE_NAME;
}
