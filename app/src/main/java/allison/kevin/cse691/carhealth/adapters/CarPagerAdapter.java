package allison.kevin.cse691.carhealth.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import allison.kevin.cse691.carhealth.fragments.CarFragment;
import allison.kevin.cse691.carhealth.fragments.MyMaintenaceFragment;
import allison.kevin.cse691.carhealth.fragments.ScheduleFragment;

/**
 * Created by kevin on 3/16/16.
 *
 * A basic ViewPager adapter
 */
public class CarPagerAdapter extends FragmentStatePagerAdapter {

    private static final int NUM_PAGES = 3;
    private static final int OVERVIEW_PAGE = 0;
    private static final int MAINTENANCE_PAGE = 1;
    private static final int SCHEDULE_PAGE = 2;

    private int carId, modelYearId;
    private String engine, trans, uuid;

    public CarPagerAdapter(FragmentManager fm,
                           int carId, int modelYearId, String engine, String trans, String uuid) {

        super(fm);

        this.carId = carId;
        this.modelYearId = modelYearId;
        this.engine = engine;
        this.trans = trans;
        this.uuid = uuid;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case OVERVIEW_PAGE:
                return CarFragment.newInstance(uuid);
            case MAINTENANCE_PAGE:
                return MyMaintenaceFragment.newInstance(uuid, modelYearId, engine, trans);
            case SCHEDULE_PAGE:
                return ScheduleFragment.newInstance(modelYearId, engine, trans);
        }
        return null;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case OVERVIEW_PAGE:
                return "Overview";
            case MAINTENANCE_PAGE:
                return "Maintenance";
            case SCHEDULE_PAGE:
                return "Schedule";
            default:
                return "Unknown";
        }
    }
}
