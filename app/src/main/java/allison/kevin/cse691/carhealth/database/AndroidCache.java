package allison.kevin.cse691.carhealth.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import allison.kevin.cse691.car.cache.LocalCache;
import allison.kevin.cse691.car.models.Car;
import allison.kevin.cse691.car.models.Maintenance;
import allison.kevin.cse691.carhealth.database.GarageContract.GarageEntry;
import allison.kevin.cse691.carhealth.database.MaintenanceContract.MaintenanceCacheEntry;
import allison.kevin.cse691.carhealth.database.MaintenanceContract.MaintenanceEntry;

/**
 * Created by kevin on 3/13/16.
 *
 * Backend storage for Garage and cached items
 * Built with Android's SQLite database
 */
public class AndroidCache implements LocalCache {

    private SQLiteOpenHelper mDbHelper;

    public AndroidCache(Context ctx) {
        mDbHelper = new DbHelper(ctx);
    }

    @Override
    public List<Car> getMyGarage() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Columns to return
        String[] projection = {
                GarageEntry._ID,
                GarageEntry.COLUMN_MAKE,
                GarageEntry.COLUMN_MODEL,
                GarageEntry.COLUMN_YEAR,
                GarageEntry.COLUMN_STYLE,
                GarageEntry.COLUMN_MODELYEARID,
                GarageEntry.COLUMN_STYLEID,
                GarageEntry.COLUMN_ENGINE,
                GarageEntry.COLUMN_TRANSMISSION
        };

        String sortOrder = GarageEntry.COLUMN_MAKE + " ASC";

        Cursor cursor = db.query(
                GarageEntry.TABLE_NAME,
                projection,
                null, null, null, null,     // Return all, no WHERE or filter
                sortOrder
        );

        List<Car> cars = new ArrayList<>();
        if (!cursor.moveToFirst()) {
            // There are no cars in the garage, return nothing..
            return  cars;
        }

        do {
            Car car = new Car(
                    cursor.getInt(cursor.getColumnIndex(GarageEntry._ID)),
                    cursor.getString(cursor.getColumnIndex(GarageEntry.COLUMN_MAKE)),
                    cursor.getString(cursor.getColumnIndex(GarageEntry.COLUMN_MODEL)),
                    cursor.getInt(cursor.getColumnIndex(GarageEntry.COLUMN_YEAR)),
                    cursor.getString(cursor.getColumnIndex(GarageEntry.COLUMN_STYLE)),
                    cursor.getInt(cursor.getColumnIndex(GarageEntry.COLUMN_MODELYEARID)),
                    cursor.getInt(cursor.getColumnIndex(GarageEntry.COLUMN_STYLEID)),
                    cursor.getString(cursor.getColumnIndex(GarageEntry.COLUMN_ENGINE)),
                    cursor.getString(cursor.getColumnIndex(GarageEntry.COLUMN_TRANSMISSION))
            );

            cars.add(car);
        } while (cursor.moveToNext());

        cursor.close();
        db.close();

        return cars;
    }

    @Override
    public void addToGarage(Car car) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(GarageEntry.COLUMN_MAKE, car.make);
        values.put(GarageEntry.COLUMN_MODEL, car.model);
        values.put(GarageEntry.COLUMN_YEAR, car.year);
        values.put(GarageEntry.COLUMN_STYLE, car.trim);
        values.put(GarageEntry.COLUMN_MODELYEARID, car.modelYearId);
        values.put(GarageEntry.COLUMN_STYLEID, car.styleId);
        values.put(GarageEntry.COLUMN_ENGINE, car.engine);
        values.put(GarageEntry.COLUMN_TRANSMISSION, car.transmission);

        long newRowId = db.insert(GarageEntry.TABLE_NAME, "null", values);

        db.close();
    }

    @Override
    public void deleteCarFromGarage(Car car) {
        String selection = GarageEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(car.id) };

        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        db.delete(GarageEntry.TABLE_NAME, selection, selectionArgs);

        db.close();
    }

    /**
     * Returns the requested car from the Garage's cache
     *
     * @param carId    Car id (local to database, NOT modelYearId)
     *
     * @return  The requested car
     */
    @Override
    public Car getCarFromGarage(int carId) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Columns to return
        String[] projection = {
                GarageEntry._ID,
                GarageEntry.COLUMN_MAKE,
                GarageEntry.COLUMN_MODEL,
                GarageEntry.COLUMN_YEAR,
                GarageEntry.COLUMN_STYLE,
                GarageEntry.COLUMN_MODELYEARID,
                GarageEntry.COLUMN_STYLEID,
                GarageEntry.COLUMN_ENGINE,
                GarageEntry.COLUMN_TRANSMISSION
        };

        String where_clause = GarageEntry._ID + " = ?";
        String[] where_params = { String.valueOf(carId) };

        String sortOrder = GarageEntry.COLUMN_MAKE + " ASC";

        Cursor cursor = db.query(
                GarageEntry.TABLE_NAME,
                projection,
                where_clause,
                where_params,
                null, null,     // No Filter
                sortOrder
        );

        if (!cursor.moveToFirst()) {
            // There are no cars in the garage, return nothing..
            return null;
        }

        Car car = new Car(
                cursor.getInt(cursor.getColumnIndex(GarageEntry._ID)),
                cursor.getString(cursor.getColumnIndex(GarageEntry.COLUMN_MAKE)),
                cursor.getString(cursor.getColumnIndex(GarageEntry.COLUMN_MODEL)),
                cursor.getInt(cursor.getColumnIndex(GarageEntry.COLUMN_YEAR)),
                cursor.getString(cursor.getColumnIndex(GarageEntry.COLUMN_STYLE)),
                cursor.getInt(cursor.getColumnIndex(GarageEntry.COLUMN_MODELYEARID)),
                cursor.getInt(cursor.getColumnIndex(GarageEntry.COLUMN_STYLEID)),
                cursor.getString(cursor.getColumnIndex(GarageEntry.COLUMN_ENGINE)),
                cursor.getString(cursor.getColumnIndex(GarageEntry.COLUMN_TRANSMISSION))
        );

        cursor.close();
        db.close();

        return car;
    }

    @Override
    public boolean checkMaintenanceCache(int id) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Columns to return
        String[] projection = {
                MaintenanceCacheEntry._ID,
                MaintenanceCacheEntry.COLUMN_MODELYEARID
        };

        String where_clause = MaintenanceCacheEntry.COLUMN_MODELYEARID + " = ?";
        String[] where_params = {String.valueOf(id)};

        String sortOrder = MaintenanceCacheEntry.COLUMN_MODELYEARID + " ASC";

        Cursor cursor = db.query(
                MaintenanceEntry.TABLE_NAME,
                projection,
                where_clause,
                where_params,
                null, null,     // No Filter
                sortOrder
        );

        boolean result = cursor.moveToFirst();

        cursor.close();
        db.close();

        return result;
    }

    public List<Maintenance> getMaintenance(int modelYearId,
                                            @Nullable String[] filterCol,
                                            @Nullable String[] filterArgs) {
        String where_clause = MaintenanceEntry.COLUMN_MODELYEARID + " = ?";
        String[] where_params;

        if (filterCol != null && filterArgs != null) {
            for (String key : filterCol) {
                where_clause = where_clause.concat(" AND " + key + " = ?");
            }

            where_params = new String[filterArgs.length + 1];
            System.arraycopy(filterArgs, 0, where_params, 1, filterArgs.length);
        } else {
            where_params = new String[1];
        }
        where_params[0] = String.valueOf(modelYearId);

        // Columns to return
        String[] projection = {
                MaintenanceEntry._ID,
                MaintenanceEntry.COLUMN_EDMUNDS_ID,
                MaintenanceEntry.COLUMN_ENGINE,
                MaintenanceEntry.COLUMN_TRANSMISSION,
                MaintenanceEntry.COLUMN_MILEAGE,
                MaintenanceEntry.COLUMN_MONTH,
                MaintenanceEntry.COLUMN_FREQ,
                MaintenanceEntry.COLUMN_ACTION,
                MaintenanceEntry.COLUMN_ITEM,
                MaintenanceEntry.COLUMN_DESC,
                MaintenanceEntry.COLUMN_DRIVE,
                MaintenanceEntry.COLUMN_LINKED_UUID
        };

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = db.query(
                MaintenanceEntry.TABLE_NAME,
                projection,
                where_clause,
                where_params,
                null, null,     // No Filter (groupBy, having)
                null            // No sort order
        );

        List<Maintenance> maintenanceList = new ArrayList<>();
        if (!cursor.moveToFirst()) {
            // There are no cars in the garage, return nothing..
            return maintenanceList;
        }

        do {
            Maintenance m = new Maintenance(
                    cursor.getInt(cursor.getColumnIndex(MaintenanceEntry._ID)),
                    cursor.getInt(cursor.getColumnIndex(MaintenanceEntry.COLUMN_EDMUNDS_ID)),
                    cursor.getString(cursor.getColumnIndex(MaintenanceEntry.COLUMN_ENGINE)),
                    cursor.getString(cursor.getColumnIndex(MaintenanceEntry.COLUMN_TRANSMISSION)),
                    cursor.getInt(cursor.getColumnIndex(MaintenanceEntry.COLUMN_MILEAGE)),
                    cursor.getInt(cursor.getColumnIndex(MaintenanceEntry.COLUMN_MONTH)),
                    cursor.getInt(cursor.getColumnIndex(MaintenanceEntry.COLUMN_FREQ)),
                    cursor.getString(cursor.getColumnIndex(MaintenanceEntry.COLUMN_ACTION)),
                    cursor.getString(cursor.getColumnIndex(MaintenanceEntry.COLUMN_ITEM)),
                    cursor.getString(cursor.getColumnIndex(MaintenanceEntry.COLUMN_DESC)),
                    0.0f, 0.0f,
                    cursor.getString(cursor.getColumnIndex(MaintenanceEntry.COLUMN_DRIVE)),
                    "",
                    cursor.getString(cursor.getColumnIndex(MaintenanceEntry.COLUMN_LINKED_UUID))
            );
            maintenanceList.add(m);
        } while (cursor.moveToNext());

        cursor.close();
        db.close();

        return maintenanceList;

    }

    @Override
    public List<Maintenance> getMaintenance(int modelYearId) {
        return getMaintenance(modelYearId, null, null);
    }

    @Override
    public void cacheMaintenance(int carId, List<Maintenance> list) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        for (Maintenance m : list) {
            ContentValues values = new ContentValues();
            values.put(MaintenanceEntry.COLUMN_EDMUNDS_ID, m.id);
            values.put(MaintenanceEntry.COLUMN_MODELYEARID, carId);
            values.put(MaintenanceEntry.COLUMN_ENGINE, m.engineCode);
            values.put(MaintenanceEntry.COLUMN_TRANSMISSION, m.transmissionCode);
            values.put(MaintenanceEntry.COLUMN_MILEAGE, m.intervalMileage);
            values.put(MaintenanceEntry.COLUMN_MONTH, m.intervalMonth);
            values.put(MaintenanceEntry.COLUMN_FREQ, m.frequency);
            values.put(MaintenanceEntry.COLUMN_ACTION, m.action);
            values.put(MaintenanceEntry.COLUMN_ITEM, m.item);
            values.put(MaintenanceEntry.COLUMN_DESC, m.itemDescription);
            values.put(MaintenanceEntry.COLUMN_DRIVE, m.driveType);
            values.put(MaintenanceEntry.COLUMN_LINKED_UUID, "");

            db.insert(MaintenanceEntry.TABLE_NAME, null, values);
        }

        ContentValues values = new ContentValues();
        values.put(MaintenanceCacheEntry.COLUMN_MODELYEARID, carId);
        db.insert(MaintenanceCacheEntry.TABLE_NAME, null, values);

        db.close();
    }

    public void associateMaintenanceActivity(long edmundsId, String uuid) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(MaintenanceEntry.COLUMN_LINKED_UUID, uuid);
        db.update(MaintenanceEntry.TABLE_NAME, values,
                MaintenanceEntry.COLUMN_EDMUNDS_ID + " = ?",
                new String[] {String.valueOf(edmundsId)});

        db.close();
    }

    public void deleteCar(String id) {

    }

    public void deleteMaintenance(String uuid) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        int affected = db.delete(
                MaintenanceEntry.TABLE_NAME,
                MaintenanceEntry.COLUMN_LINKED_UUID + " = ?",
                new String[] { uuid }
        );

        String debug = String.format(
                Locale.getDefault(),
                "Deleted %d rows (%s)",
                affected, uuid
        );
        Log.d("CarHealth", debug);

        db.close();
    }
}
