package allison.kevin.cse691.carhealth.viewholders;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import allison.kevin.cse691.car.models.Maintenance;
import allison.kevin.cse691.carhealth.R;
import allison.kevin.cse691.carhealth.util.CarFirebase;

/**
 * Created by kevin on 4/16/16.
 *
 * Represents the list of items required for a given interval
 */
public class ScheduleItemViewHolder extends ChildViewHolder implements View.OnClickListener{
    public View parentView;
    private TextView txtCategory;
    private TextView txtName;
    private TextView txtFreq;
    private TextView txtLast;
    private View doneView;

    public ScheduleItemViewHolder(View view, View.OnClickListener onClickListener) {
        super(view);

        //this.callback = callback;

        this.parentView = view;
        //this.parentView.setClickable(true);
        txtName = (TextView)view.findViewById(R.id.sched_item);
        txtCategory = (TextView)view.findViewById(R.id.sched_action);
        txtFreq = (TextView)view.findViewById(R.id.sched_freq);
        txtLast = (TextView)view.findViewById(R.id.sched_mileage);
        doneView = view.findViewById(R.id.sched_done);

        if (onClickListener != null) {
            view.setOnClickListener(onClickListener);
        }
    }

    public void bind(Maintenance maintenance) {
        parentView.setTag(maintenance.edmundsId);
        parentView.setId(maintenance.id);
        txtName.setText(maintenance.item);
        txtCategory.setText(maintenance.action);
        txtFreq.setText(maintenance.interval());

        if (!maintenance.linkedUuid.equals("")) {
            // This has been completed

            //txtDate.setText("19 Apr 2016");
            //txtMiles.setText("120,000 miles");
            doneView.setVisibility(View.VISIBLE);
        } else {
            doneView.setVisibility(View.GONE);
        }
    }

    public void bind(Maintenance maintenance, String carUuid) {
        bind(maintenance);

        try {
            if (CarFirebase.isLoggedIn()) {
                CarFirebase.getMaintenanceForEdmundsId(maintenance.edmundsId, carUuid,
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onCancelled(FirebaseError firebaseError) {

                            }
                        });
            }
        } catch (Exception e) {
            Log.d("CarHealth", "bind(Schedule): " + e.getMessage());
        }
    }

    public void setCompleted(String date) {
        //txtDate.setText("19 Apr 2016");
        //txtMiles.setText("120,000 miles");
        doneView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        //callback.swapFragment(CarFragment.newInstance(
        //       cache.getMyGarage().get(getAdapterPosition()).id));
        Log.d("CarHealth", "Clicked!");
        view.setSelected(true);
    }
}
