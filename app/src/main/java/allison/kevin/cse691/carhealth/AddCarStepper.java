package allison.kevin.cse691.carhealth;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import allison.kevin.cse691.car.EdmundsApi;
import allison.kevin.cse691.car.holders.MakeHolder;
import allison.kevin.cse691.car.models.Car;
import allison.kevin.cse691.car.models.FullStyle;
import allison.kevin.cse691.car.models.Make;
import allison.kevin.cse691.car.models.Model;
import allison.kevin.cse691.car.models.Style;
import allison.kevin.cse691.car.models.Year;
import allison.kevin.cse691.carhealth.adapters.SimpleAdapter;
import allison.kevin.cse691.carhealth.util.CarFirebase;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kevin on 3/19/16.
 *
 * Material design stepper for adding cars
 */
public class AddCarStepper extends AppCompatActivity {

    private Make selectedMake;
    private Model selectedModel;
    private Year selectedYear;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_add_car);

        // TextView Headers
        final TextView makeHeader = (TextView)findViewById(R.id.make_header);
        final TextView modelHeader = (TextView)findViewById(R.id.model_header);
        final TextView yearHeader = (TextView)findViewById(R.id.year_header);
        final TextView styleHeader = (TextView)findViewById(R.id.style_header);

        // Recycler Views
        final RecyclerView makeList = (RecyclerView)findViewById(R.id.make_list);
        final RecyclerView modelList = (RecyclerView)findViewById(R.id.model_list);
        final RecyclerView yearList = (RecyclerView)findViewById(R.id.year_list);
        final RecyclerView styleList = (RecyclerView)findViewById(R.id.style_list);

        // Misc Views
        final View makeExpandView = findViewById(R.id.make_expand_view);
        final View styleExpandView = findViewById(R.id.style_expand_view);
        final View makeProgess = findViewById(R.id.make_progess);
        final View styleProgress = findViewById(R.id.style_progess);


        /**
         * Control hiding/showing expandable views in the adapters
         * The general sequence of events is:
         *  1) Add new data to correct adapter
         *  2) Hide the currently expanded view by setting its visibility to GONE
         *  3) Show the next view by setting its visibility to VISIBLE
         */

        final SimpleAdapter<Style> styleAdapter = new SimpleAdapter<>(new SimpleAdapter.SimpleAdapterCallback<Style>() {
            @Override
            public void onClick(final Style selectedStyle) {
                if (selectedStyle == null) {
                    return;
                }

                styleProgress.setVisibility(View.VISIBLE);

                // Style is the last step, add the car after completion
                EdmundsApi.getStyleAsync(EdmundsApi.getApi(),
                        selectedStyle.id, new Callback<FullStyle>() {
                            @Override
                            public void onResponse(Call<FullStyle> call, Response<FullStyle> response) {
                                FullStyle fullStyle = response.body();

                                Car car = new Car(0, selectedMake.name, selectedModel.name, selectedYear.year,
                                        selectedStyle.trim, selectedYear.id, selectedStyle.id,
                                        fullStyle.engine.code, fullStyle.transmission.transmissionType);
                                try {
                                    CarFirebase.addCarToCurrentUserRef(car);
                                } catch (Exception e) {
                                    Log.e("CarHealth", "User Ref is null, cannot add car");
                                }

                                finish();
                            }

                            @Override
                            public void onFailure(Call<FullStyle> call, Throwable t) {
                                Log.e("CarHealth", "Problem Adding Car to Garage: " + t.getMessage());
                            }
                        });
            }
        });

        final SimpleAdapter<Year> yearAdapter = new SimpleAdapter<>(new SimpleAdapter.SimpleAdapterCallback<Year>() {
            @Override
            public void onClick(Year data) {
                yearHeader.setText(String.valueOf(data.year));
                yearList.setVisibility(View.GONE);
                styleExpandView.setVisibility(View.VISIBLE);
                styleProgress.setVisibility(View.VISIBLE);
                // Load styles then add to style adapter
                EdmundsApi.getModelYearInfoAsync(EdmundsApi.getApi(),
                        selectedMake, // Make
                        selectedModel, // Model
                        data.year, // Year
                        new Callback<Year>() {
                            @Override
                            public void onResponse(Call<Year> call, Response<Year> response) {
                                selectedYear = response.body();

                                if (selectedYear != null) {
                                    styleAdapter.setData(selectedYear.styles);
                                    styleProgress.setVisibility(View.GONE);
                                } else {
                                    Log.d("CarHealth", "Null style data!");
                                }
                            }

                            @Override
                            public void onFailure(Call<Year> call, Throwable t) {
                                Log.d("CarHealth", "Error getting style data: " + t.getMessage());
                            }
                        }
                );
            }
        });

        final SimpleAdapter<Model> modelAdapter = new SimpleAdapter<>(new SimpleAdapter.SimpleAdapterCallback<Model>() {
            @Override
            public void onClick(Model data) {
                selectedModel = data;

                // Add years to year adapter
                modelHeader.setText(data.toString());
                modelList.setVisibility(View.GONE);
                yearList.setVisibility(View.VISIBLE);

                yearAdapter.setData(data.years);
            }
        });

        final SimpleAdapter<Make> makeAdapter = new SimpleAdapter<>(new SimpleAdapter.SimpleAdapterCallback<Make>() {
            @Override
            public void onClick(Make make) {
                selectedMake = make;

                // Add models to model adapter
                makeHeader.setText(make.toString());
                makeExpandView.setVisibility(View.GONE);
                modelList.setVisibility(View.VISIBLE);

                modelAdapter.setData(make.models);

            }
        });

        // Set the RecyclerView adapters
        makeList.setAdapter(makeAdapter);
        modelList.setAdapter(modelAdapter);
        yearList.setAdapter(yearAdapter);
        styleList.setAdapter(styleAdapter);

        // Set the RecyclerView layout managers
        makeList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        modelList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        yearList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        styleList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        // On Click callbacks
        /*
        makeHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (makeExpandView.getVisibility() != View.VISIBLE) {
                    modelList.setVisibility(View.GONE);
                    yearList.setVisibility(View.GONE);
                    styleExpandView.setVisibility(View.GONE);
                    makeExpandView.setVisibility(View.VISIBLE);
                }
            }
        });

        modelHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modelList.getVisibility() != View.VISIBLE) {
                    makeExpandView.setVisibility(View.GONE);
                    yearList.setVisibility(View.GONE);
                    styleExpandView.setVisibility(View.GONE);
                    modelList.setVisibility(View.VISIBLE);
                }
            }
        });

        yearHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (yearList.getVisibility() != View.VISIBLE) {
                    makeExpandView.setVisibility(View.GONE);
                    modelList.setVisibility(View.GONE);
                    styleExpandView.setVisibility(View.GONE);
                    yearList.setVisibility(View.VISIBLE);
                }
            }
        });
        */


        // Load the different car make/model/year information
        EdmundsApi.getMakesAsync(EdmundsApi.getApi(), new Callback<MakeHolder>() {
            @Override
            public void onResponse(Call<MakeHolder> call, Response<MakeHolder> response) {
                MakeHolder holder = response.body();

                makeAdapter.setData(holder.makes);

                // Hide the progress view now that we've retrieved the data
                makeProgess.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<MakeHolder> call, Throwable t) {
                Log.e("AddCar", "Failed to get car manufacturers: " + t.getMessage());
                Snackbar.make(makeList,
                        "Failed to get car manufacturers: " + t.getMessage(),
                        Snackbar.LENGTH_LONG).show();
            }
        });
    }
}
