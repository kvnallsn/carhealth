package allison.kevin.cse691.carhealth.util;

import android.content.Context;
import android.util.Log;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;

import java.util.Locale;

import allison.kevin.cse691.car.models.Car;
import allison.kevin.cse691.car.models.Maintenance;
import allison.kevin.cse691.carhealth.models.CompletedItem;

/**
 * Created by kevin on 4/10/16.
 *
 * Singleton to access the application's Firebase reference
 *
 * Firebase design:
 * + User UUID (e.g. google:91102931201)
 *   + Cars
 *      + Car UUID (e.g. -AJKAKJAJFKA)
 *          + make
 *          + model
 *          + ...
 *      + Car UUID
 *      + ...
 *   + Maintenance
 *      + Maintenance UUID (e.g. -AKSKDFKS)
 *          + Car UUID (e.g. -AJKAKJAJFKA)
 *          + Edmunds Ids:
 *              + 395009
 *              + 395010
 *              + ...
 *          + Type
 *          + Date
 *          + ...
 *      + Maintenance UUID
 *      + ...
 *   + Completed
 *      + Completed UUID
 *          + Edmunds ID_Car UUID (395009_-AJKAKJAJFKA)
 *              + Maintenance UUIDs (e.g. -AKSKDFKS)
 *      + ...
 *  + User UUID
 *  + ...
 */
public class CarFirebase {

    private static CarFirebase carFirebase = null;
    public static String URL = "https://carhealth.firebaseio.com/";

    public static void init(Context context) { //}, String url) {
        if (carFirebase == null) {
            carFirebase = new CarFirebase(context, URL);
        }
    }

    public static Firebase getCar(String uuid) throws Exception {
        Firebase carRef = getCurrentUserCarsRef();
        return carRef.child(uuid);
    }

    public static void deleteCar(String uuid) throws Exception {
        Firebase carsRef = CarFirebase.getCar(uuid);
        carsRef.removeValue();


    }

    public static Firebase getFirebaseRef() {
        return carFirebase.ref;
    }

    public static Firebase getCurrentUserRef() throws Exception {
        return carFirebase.getUserRef();
    }

    public static Firebase getCurrentUserCarsRef() throws Exception {
        Firebase userRef = carFirebase.getUserRef();
        if (userRef != null) {
            return userRef.child("cars");
        }

        throw new Exception("getCurrentUserCarsRef(): userRef is null");
    }

    public static Firebase getCurrentUserMaintRef() throws Exception {
        Firebase userRef = carFirebase.getUserRef();
        if (userRef != null) {
            return userRef.child("maintenance");
        }

        throw new Exception("getCurrentUserMaintRef(): userRef is null");
    }

    // Get the link between schedule items and user completed maintenance
    public static Query getCurrentUserCompletedRef(int edmundsId, String carUuid) throws Exception {
        Firebase userRef = carFirebase.getUserRef();
        if (userRef != null) {
            String fmt = String.format(Locale.ENGLISH, "%d_%s", edmundsId, carUuid);
            return userRef.child("completed").orderByChild("id").equalTo(fmt);
        }

        throw new Exception("getCurrentUserCompletedRef(): userRef is null");
    }

    public static String addCarToCurrentUserRef(Car car) throws Exception {
        Firebase newRef = getCurrentUserCarsRef().push();
        car.setUuid(newRef.getKey());
        newRef.setValue(car);
        return newRef.getKey();
    }

    public static void getMaintenanceForEdmundsId(
            int edmunds, String carUuid, ValueEventListener listener) {
        try {
            Query ref = getCurrentUserCompletedRef(edmunds, carUuid);
            ref.addListenerForSingleValueEvent(listener);
        } catch (Exception e) {
            Log.d("CarHealth", e.getLocalizedMessage());
        }
    }

    public static void addMaintenanceForEdmundsId(
            int edmunds, String carUuid, Maintenance maintenance) {

        try {
            CompletedItem item = new CompletedItem(edmunds, carUuid);
            Query query = getCurrentUserCompletedRef(edmunds, carUuid);

            query.getRef().push().setValue("");
        } catch (Exception e) {
            Log.d("CarHealth", "Add Maintenance: " + e.getMessage());
        }
    }

    public static Firebase getRef(String path) {
        return carFirebase.ref.child(path);
    }

    private Firebase ref;

    private CarFirebase(Context context, String url) {
        Firebase.setAndroidContext(context);
        Firebase.getDefaultConfig().setPersistenceEnabled(true);
        ref = new Firebase(url);
    }

    public static boolean isLoggedIn() throws Exception {
        if (carFirebase.ref.getAuth() == null) {
            throw new Exception("User not logged in!");
        }

        return true;
    }

    public static String getUserName() {
        String mName = "";
        try {
            if (isLoggedIn()) {
                AuthData authData = carFirebase.ref.getAuth();
                switch (authData.getProvider()) {
                    case "facebook":
                    case "google":
                        mName = (String) authData.getProviderData().get("displayName");
                        break;
                    case "password":
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            Log.d("CarHealth", "User not logged in!");
        }
        return mName;
    }

    public static String getUserEmail() {
        String email = "None";
        try {
            if (isLoggedIn()) {
                AuthData authData = carFirebase.ref.getAuth();
                switch (authData.getProvider()) {
                    case "facebook":
                    case "google":
                    case "password":
                        email = (String) authData.getProviderData().get("email");
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            Log.d("CarHealth", "User not logged in!");
        }

        return email;
    }

    public static String getUserImageUrl() {
        String url = "";
        try {
            if (isLoggedIn()) {
                AuthData authData = carFirebase.ref.getAuth();
                switch (authData.getProvider()) {
                    case "facebook":
                    case "google":
                        url = (String) authData.getProviderData().get("profileImageURL");
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            Log.d("CarHealth", "User not logged in!");
        }
        return url;
    }

    public static void logout() {
        try {
            if (isLoggedIn()) {
                carFirebase.ref.unauth();
            }
        } catch (Exception e) {
            Log.d("CarHealth", "User not logged in!");
        }
    }

    private Firebase getUserRef() throws Exception  {
        if (isLoggedIn()) {
            AuthData authData = ref.getAuth();
            return ref.child(authData.getUid());
        }

        return null;
    }
}
