package allison.kevin.cse691.carhealth.callbacks;

/**
 * Created by kevin on 4/17/16.
 *
 * A simple way to communicate from fragment to activity
 */
public interface FragmentCallback {
    void cancelActivity();
}
