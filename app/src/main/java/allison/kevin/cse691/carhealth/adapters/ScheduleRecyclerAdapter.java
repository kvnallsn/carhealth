package allison.kevin.cse691.carhealth.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import allison.kevin.cse691.car.models.Maintenance;
import allison.kevin.cse691.carhealth.R;
import allison.kevin.cse691.carhealth.models.MaintenanceInterval;
import allison.kevin.cse691.carhealth.viewholders.IntervalViewHolder;
import allison.kevin.cse691.carhealth.viewholders.ScheduleItemViewHolder;

/**
 * Created by kevin on 3/13/16.
 *
 * RecyclerView adapter for the Garage
 */
public class ScheduleRecyclerAdapter extends ExpandableRecyclerAdapter<IntervalViewHolder, ScheduleItemViewHolder> {

    private LayoutInflater mInflater;
    private View.OnClickListener onClickListener;
    private ArrayList<String> selectedItems;
    private Boolean isStepperAdapter;
    //private int carId;

    /**
     * Construct the adapter
     *
     * @param ctx               Operating context, generally the application context
     * @param parentItemList    List of items for the parent list
     * @param stepper           True if this is in a stepper
     */
    public ScheduleRecyclerAdapter(Context ctx,
                                   @NonNull List<? extends ParentListItem> parentItemList,
                                   boolean stepper) {
        super(parentItemList);
        this.mInflater = LayoutInflater.from(ctx);
        this.selectedItems = new ArrayList<>();
        this.isStepperAdapter = stepper;

        if (isStepperAdapter) {
            onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String edmundsId = v.getTag().toString();

                    if (selectedItems.contains(edmundsId)) {
                        v.setSelected(false);
                        selectedItems.remove(edmundsId);
                    } else {
                        v.setSelected(true);
                        selectedItems.add(edmundsId);
                    }
                }
            };
        }
    }

    public void resetItems(@NonNull List<MaintenanceInterval> newItems) {
        List<? extends ParentListItem> parentListItems = getParentItemList();
        Iterator<? extends ParentListItem> iter = parentListItems.iterator();
        while (iter.hasNext()) {
            ParentListItem item = iter.next();
            int index = parentListItems.indexOf(item);
            iter.remove();
            notifyParentItemRemoved(index);
        }

        ((List<MaintenanceInterval>)getParentItemList()).addAll(newItems);
        int numItems = getParentItemList().size();
        notifyParentItemRangeInserted(0, numItems);
    }

    public ArrayList<String> getSelectedItems() {
        return selectedItems;
    }

    @Override
    public IntervalViewHolder onCreateParentViewHolder(ViewGroup parent) {
        View intervalView = mInflater.inflate(R.layout.maintenance_holder, parent, false);
        return new IntervalViewHolder(intervalView);
    }

    @Override
    public ScheduleItemViewHolder onCreateChildViewHolder(ViewGroup parent) {
        View maintenanceView = mInflater.inflate(R.layout.schedule_item, parent, false);
        return new ScheduleItemViewHolder(maintenanceView, onClickListener);
    }

    @Override
    public void onBindParentViewHolder(IntervalViewHolder intervalViewHolder,
                                       int position, ParentListItem parentListItem) {

        MaintenanceInterval intervalItem = (MaintenanceInterval)parentListItem;
        intervalViewHolder.bind(intervalItem);
    }

    @Override
    public void onBindChildViewHolder(ScheduleItemViewHolder scheduleItemViewHolder,
                                      int position, Object childListItem) {

        Maintenance maintenance = (Maintenance)childListItem;
        if (isStepperAdapter) {
            scheduleItemViewHolder.bind(maintenance);
        } else {
            scheduleItemViewHolder.bind(maintenance, null);
        }
    }
}
