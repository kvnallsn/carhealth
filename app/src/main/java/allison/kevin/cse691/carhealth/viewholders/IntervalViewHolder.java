package allison.kevin.cse691.carhealth.viewholders;

import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

import allison.kevin.cse691.carhealth.R;
import allison.kevin.cse691.carhealth.models.MaintenanceInterval;

/**
 * Created by kevin on 4/16/16.
 *
 * Represents the interval (mileage/month) for the schedule fragment
 */
public class IntervalViewHolder extends ParentViewHolder {
    private TextView holderText;
    /**
     * Default constructor.
     *
     * @param itemView The {@link View} being hosted in this ViewHolder
     */
    public IntervalViewHolder(View itemView) {
        super(itemView);
        holderText = (TextView)itemView.findViewById(R.id.maintenance_holder_text);
    }

    public void bind(MaintenanceInterval item) {
        holderText.setText(item.interval);
    }
}
