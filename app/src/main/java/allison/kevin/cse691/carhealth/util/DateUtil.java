package allison.kevin.cse691.carhealth.util;

import java.util.Calendar;

/**
 * Created by kevin on 4/13/16.
 *
 * Class to help with storing dates on Firebase
 * Because Firebase is so limited (can't even offer a DESC option!), we have to stored the
 * timestamps inverted
 */
public class DateUtil {

    public static Calendar retrieveTimeStamp(long ts) {
        Calendar calendar = Calendar.getInstance();
        if (calendar != null) {
            calendar.setTimeInMillis(ts);
        }

        return calendar;
    }
}
