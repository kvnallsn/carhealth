package allison.kevin.cse691.carhealth;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.firebase.client.Firebase;

import java.util.List;

import allison.kevin.cse691.car.EdmundsApi;
import allison.kevin.cse691.carhealth.database.AndroidCache;
import allison.kevin.cse691.carhealth.fragments.ScheduleFragment;
import allison.kevin.cse691.carhealth.fragments.steps.AddMaintenanceFragment;
import allison.kevin.cse691.carhealth.fragments.steps.CalendarFragment;
import allison.kevin.cse691.carhealth.fragments.steps.StepperFragment;
import allison.kevin.cse691.carhealth.models.MaintenanceItem;
import allison.kevin.cse691.carhealth.util.CarFirebase;

/**
 * Created by kevin on 4/16/16.
 *
 * A stepper used to add different maintenance items
 */
public class AddMaintenanceStepper extends StepperActivity {

    private static final int INFO_PAGE = 0;
    private static final int LINK_PAGE = 1;
    private static final int CALENDAR_PAGE = 2;

    MaintenanceItem maintenanceItem;

    @Override
    protected void onStepperInit(List<Class<? extends StepperFragment>> fragmentList) {
        fragmentList.add(AddMaintenanceFragment.class);
        fragmentList.add(ScheduleFragment.class);
        fragmentList.add(CalendarFragment.class);

        maintenanceItem = new MaintenanceItem();

        Intent intent = getIntent();
        if (intent != null) {
            Bundle args = new Bundle();
            args.putInt("modelYearId", intent.getIntExtra("modelYearId", 0));
            args.putString("engine", intent.getStringExtra("engine"));
            args.putString("trans", intent.getStringExtra("trans"));
            setFragmentArgs(args);

            maintenanceItem.setCarUuid(getIntent().getStringExtra("uuid"));
        }
    }

    @Override
    protected void onNextStep(Bundle results) {
        int step = results.getInt("step", -1);
        switch (step) {
            case INFO_PAGE:
                maintenanceItem.setCost(results.getDouble("cost"));
                maintenanceItem.setCategory(results.getString("category"));
                maintenanceItem.setDescription(results.getString("description"));
                maintenanceItem.setMileage(results.getDouble("mileage"));
                break;
            case LINK_PAGE:
                maintenanceItem.setEdmundsIds(results.getStringArrayList("items"));
                break;
            case CALENDAR_PAGE:
                maintenanceItem.setDate(results.getLong("date"));
                Log.d("CarHealth", "Date: "+ results.getLong("date"));
                break;
            default:
                Log.d("CarHealth", "Unknown Step: " + step);
        }
    }

    @Override
    protected void onStepperFinish() {
        try {
            Firebase ref = CarFirebase.getCurrentUserMaintRef().push();
            ref.setValue(maintenanceItem);

            for (String mItem : maintenanceItem.getEdmundsIds()) {
                Log.d("CarHealth", "Associating: " + mItem + " -> " + ref.getKey());
                ((AndroidCache)EdmundsApi.getCache()).associateMaintenanceActivity(
                        Long.parseLong(mItem),
                        ref.getKey()
                );
            }
        } catch (Exception e) {
            Log.e("CarHealth", "User Ref is Null. Cannot add maintenance");
        }
    }
}
