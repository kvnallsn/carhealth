package allison.kevin.cse691.carhealth.models;

import java.util.List;

/**
 * Created by kevin on 4/10/16.
 *
 * POJO for a maintenance item
 */
public class MaintenanceItem {

    private String carUuid;
    private String category;
    private long date;
    private double cost;
    private String description;
    private double mileage;
    //private long edmundsId;
    private List<String> edmundsIds;

    public MaintenanceItem() {
        // Default Empty Constructor
    }

    public String getCarUuid() { return this.carUuid; }
    public String getCategory() { return this.category; }
    public long getDate() { return this.date; }
    public double getCost() { return this.cost; }
    public String getDescription() { return this.description; }
    public double getMileage() { return  this.mileage; }
    //public long getEdmundsId() { return this.edmundsId; }
    public List<String> getEdmundsIds() { return this.edmundsIds; }

    public void setCarUuid(String uuid) { this.carUuid = uuid; }
    public void setCategory(String category) { this.category = category; }
    public void setDate(long date) { this.date = date; }
    public void setCost(double cost) { this.cost = cost; }
    public void setDescription(String description) { this.description = description; }
    public void setMileage(double mileage) { this.mileage = mileage; }
    //public void setEdmundsId(long edmundsId) { this.edmundsId = edmundsId; }
    public void setEdmundsIds(List<String> edmundsIds) { this.edmundsIds = edmundsIds; }
}
