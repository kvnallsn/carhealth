package allison.kevin.cse691.carhealth.adapters;

import android.content.Context;

import com.firebase.client.Firebase;
import com.firebase.client.annotations.NotNull;
import com.firebase.ui.FirebaseRecyclerAdapter;

import allison.kevin.cse691.car.models.Car;
import allison.kevin.cse691.carhealth.callbacks.GarageCallback;
import allison.kevin.cse691.carhealth.viewholders.GarageViewHolder;

/**
 * Created by kevin on 4/10/16.
 *
 * Firebase recyclerview adapter for the garage fragment
 */
public class GarageFirebaseAdapter extends FirebaseRecyclerAdapter<Car, GarageViewHolder> {

    private GarageCallback mCallback;

    public GarageFirebaseAdapter(Class<Car> carClass, int modelLayout,
                                 @NotNull Firebase ref, @NotNull Context ctx,
                                 @NotNull GarageCallback callback) {
        super(carClass, modelLayout, GarageViewHolder.class, ref);

        this.mCallback = callback;
    }


    @Override
    protected void populateViewHolder(GarageViewHolder holder, Car car, int i) {
        holder.setData(car, mCallback);
    }
}
