package allison.kevin.cse691.carhealth.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import allison.kevin.cse691.car.models.Car;
import allison.kevin.cse691.carhealth.R;
import allison.kevin.cse691.carhealth.callbacks.FragmentCallback;
import allison.kevin.cse691.carhealth.util.CarFirebase;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CarFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CarFragment extends Fragment {

    private String uuid;
    private FragmentCallback fragmentCallback;

    public CarFragment() {
        // Required empty public constructor;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CarFragment.
     */
    public static CarFragment newInstance(String uuid) {
        CarFragment fragment = new CarFragment();
        Bundle args = new Bundle();
        args.putString("uuid", uuid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentCallback) {
            fragmentCallback = (FragmentCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentCallback");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (getArguments() != null) {
            uuid = getArguments().getString("uuid");
        }

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_car, container, false);

        final TextView txtYear = (TextView)view.findViewById(R.id.frag_car_year);
        final TextView txtName = (TextView)view.findViewById(R.id.frag_car_name);
        final TextView txtEngine = (TextView)view.findViewById(R.id.frag_car_engine);
        final TextView txtTrans = (TextView)view.findViewById(R.id.frag_car_trans);
        //final TextView txtColor = (TextView)view.findViewById(R.id.frag_car_color);
        final ImageView imgTrans = (ImageView)view.findViewById(R.id.frag_car_trans_image);

        // Get the car to display info about
        try {
            CarFirebase.isLoggedIn();
        } catch (Exception e) {
            Log.e("CarHealth", "CarFragment: User is Not Logged In");
            fragmentCallback.cancelActivity();
            return view;
        }

        try {
            Firebase userRef = CarFirebase.getCurrentUserCarsRef();
            if (userRef != null) {
                Firebase ref = userRef.child(uuid);
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Car car = dataSnapshot.getValue(Car.class);

                        txtYear.setText(String.valueOf(car.getYear()));
                        txtName.setText(car.toString());
                        txtEngine.setText(car.getEngine());
                        //txtTrans.setText(car.getTransmission());

                        if (car.getTransmission().equals("MANUAL")) {
                            imgTrans.setImageResource(R.drawable.ic_manual);
                            txtTrans.setText("Manual");
                        } else {
                            imgTrans.setImageResource(R.drawable.ic_auto);

                            if (car.getTransmission().equals("AUTOMATIC")) {
                                txtTrans.setText("Automatic");
                            } else if (car.getTransmission().equals("AUTOMATIC MANUAL")) {
                                txtTrans.setText("Shiftable Auto");
                            } else {
                                txtTrans.setText(car.getTransmission());
                            }
                        }
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {
                        Log.e("CarHealth", "Failed to load car data: " + firebaseError.getMessage());
                        Snackbar.make(view, "Failed to load car data", Snackbar.LENGTH_LONG).show();
                    }
                });
            }
        } catch (Exception e) {
            Log.e("CarHealth", "CarFragment: Error Loading Current User's Car Ref");
            getActivity().finish();
        }

        return view;
    }
}
