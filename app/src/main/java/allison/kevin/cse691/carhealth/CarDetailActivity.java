package allison.kevin.cse691.carhealth;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import allison.kevin.cse691.carhealth.adapters.CarPagerAdapter;
import allison.kevin.cse691.carhealth.callbacks.FragmentCallback;

public class CarDetailActivity extends AppCompatActivity implements FragmentCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_detail);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /* Get car information out of intent */
        Intent intent = getIntent();
        int carId = intent.getIntExtra("carId", 0);
        int modelYearId = intent.getIntExtra("modelYearId", 0);
        String engine = intent.getStringExtra("engine");
        String trans = intent.getStringExtra("trans");
        String uuid = intent.getStringExtra("uuid");
        String makeModel = intent.getStringExtra("car");

        setTitle(makeModel);

        ViewPager viewPager = (ViewPager)findViewById(R.id.viewPager);
        TabLayout tabs = (TabLayout)findViewById(R.id.tabs);

        viewPager.setAdapter(new CarPagerAdapter(getSupportFragmentManager(),
                carId, modelYearId, engine, trans, uuid));

        tabs.setupWithViewPager(viewPager);
    }

    @Override
    public void cancelActivity() {
        finish();
    }
}
