package allison.kevin.cse691.carhealth.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;

/**
 * Created by kevin on 4/2/16.
 *
 * Helper class to aid in getting the user's location
 */
public class LocationUtil {

    // Permissions Request
    public static final int REQUEST_FINE_LOCATION_PERMISSION = 0;

    ///////////////////////////////////////
    // Singleton Methods
    ///////////////////////////////////////
    private static LocationUtil singleton;

    public static void init(WeakReference<Activity> activity) {
        if (singleton == null) {
            singleton = new LocationUtil(activity);
        }
    }

    public static LocationUtil get() {
        return singleton;
    }

    // Local variables
    private LocationManager locationManager;
    private WeakReference<Activity> activity;
    private boolean mockData;
    private Location lastLocation;

    // Test Lat/Long since GTMO has no results. Go figure
    public static final double ROME_LATITUDE = 43.2194;
    public static final double ROME_LONGITUDE = -75.4633;

    private LocationUtil(WeakReference<Activity> activity) {
        this.activity = activity;
        this.mockData = false;
        this.lastLocation = null;
    }

    public void useMockData(boolean mockData) {
        this.mockData = mockData;
    }

    ///////////////////////////////////////////////////
    // Helper Location Methods
    ///////////////////////////////////////////////////
    public void setLastLocation(Location lastLocation) {
        this.lastLocation = lastLocation;
    }

    public Location getLastLocation() {
        return this.lastLocation;
    }

    public void getCity(final double latitude, final double longitude) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(activity.get().getApplicationContext(),
                        Locale.getDefault());
                try {
                    List<Address> addressList = geocoder.getFromLocation(ROME_LATITUDE, ROME_LONGITUDE, 10);
                    if (addressList.size() > 0) {
                        Log.d("CarHealth", "" + addressList.get(0).getLocality());
                        Log.d("CarHealth", "" + addressList.get(0).getCountryName());
                        Log.d("CarHealth", "" + addressList.get(0).getSubLocality());
                    }
                } catch (IOException ex) {
                    Log.e("CarHealth", "Error getting city name: " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
        }).start();
    }

    public boolean hasPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(
                activity.get().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);

        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        if (ActivityCompat.shouldShowRequestPermissionRationale(activity.get(),
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Show rationale async, do not block!
            return false;
        }

        // Request the permission
        ActivityCompat.requestPermissions(
                activity.get(),
                new String[] { Manifest.permission.ACCESS_FINE_LOCATION },
                REQUEST_FINE_LOCATION_PERMISSION
        );

        return false;
    }

    ///////////////////////////////////////////////////
    // Non-Google Play Location Services
    ///////////////////////////////////////////////////

    public void initAndroidService() {
        locationManager = (LocationManager)
                activity.get().getSystemService(Context.LOCATION_SERVICE);
    }

    // hasPermission() checks the appropriate setting and requests it if necessary
    // so suppress any warning about missing permissions
    @SuppressWarnings({"MissingPermission"})
    public Location getLastKnownLocationAndroid() {

        if (mockData) {
            Location mock = new Location(LocationManager.GPS_PROVIDER);
            mock.setLatitude(ROME_LATITUDE);
            mock.setLongitude(ROME_LONGITUDE);

            return mock;
        }

        if (hasPermission()) {
            return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        } else {
            Log.d("CarHealth", "Permission Denied!");
        }

       return null;
    }

    // hasPermission() checks the appropriate setting and requests it if necessary
    // so suppress any warning about missing permissions
    @SuppressWarnings({"MissingPermission"})
    public void updateAndroidLocation() {

        if (mockData) {
            return;
        }

        final android.location.LocationListener locationListener = new android.location.LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                String loc = String.format(Locale.ENGLISH, "%f %f",
                        location.getLatitude(), location.getLongitude());
                Log.d("CarHealth", "Location Changed: " + loc);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.d("CarHealth", provider + " status change! (" + status + ")");
            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.d("CarHealth", provider + " enabled!");
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.d("CarHealth", provider + " disabled!");
            }
        };

        if (hasPermission()) {
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }
    }

    public static String prettyPrintLatLong(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        return String.format(Locale.getDefault(), "%s %f, %s %f",
                (latitude < 0.0) ? "S" : "N",
                (latitude < 0.0) ? (latitude * -1.0) : latitude,
                (longitude < 0.0) ? "W" : "E",
                (longitude < 0.0) ? (longitude * -1.0) : longitude);
    }
}
