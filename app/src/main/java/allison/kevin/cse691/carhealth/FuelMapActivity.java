package allison.kevin.cse691.carhealth;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.lang.ref.WeakReference;

import allison.kevin.cse691.carhealth.adapters.FuelMarkerAdapter;
import allison.kevin.cse691.carhealth.util.FuelUtil;
import allison.kevin.cse691.carhealth.util.LocationUtil;
import allison.kevin.cse691.gas.models.Station;

public class FuelMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private int id;
    private boolean showList;
    private Location location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fuel_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        location = LocationUtil.get().getLastLocation();
        Intent intent = getIntent();

        if (intent.hasExtra("id")) {
            showList = false;
            id = intent.getIntExtra("id", 0);
        } else {
            showList = true;
        }


        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        // Add a marker in Sydney and move the camera
        LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(true);
        googleMap.setInfoWindowAdapter(new FuelMarkerAdapter(new WeakReference<Activity>(this)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 13));

        if (showList) {
            // Add all fuel stations from list
            int i = 0;
            for (Station station : FuelUtil.get().getStationList()) {
                LatLng loc = new LatLng(station.lat, station.lng);

                googleMap.addMarker((new MarkerOptions())
                        .position(loc)
                        .snippet(String.valueOf(i)));
                i++;
            }
        } else {
            // Mark the POI passed
            Station poi = FuelUtil.get().getStation(id);
            googleMap.addMarker((new MarkerOptions())
                        .position(new LatLng(poi.lat, poi.lng))
                        .snippet(String.valueOf(id)));
        }
    }
}
