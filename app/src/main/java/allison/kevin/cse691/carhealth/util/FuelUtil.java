package allison.kevin.cse691.carhealth.util;

import java.util.ArrayList;
import java.util.List;

import allison.kevin.cse691.gas.models.Station;

/**
 * Created by kevin on 4/3/16.
 *
 * Class to cache fuel stations nearby
 */
public class FuelUtil {

    ///////////////////////////////////////
    // Singleton Methods
    ///////////////////////////////////////
    private static FuelUtil singleton;

    static {
        singleton = new FuelUtil();
    }

    public static FuelUtil get() {
        return singleton;
    }

    ///////////////////////////////////////
    // Class Methods
    ///////////////////////////////////////

    // Private constructor to prevent outside use
    // Forces singleton pattern
    private List<Station> fuelStations;

    private FuelUtil() {
        fuelStations = new ArrayList<>();
    }

    public void addStation(Station station) {
        fuelStations.add(station);
    }

    public void addStations(List<Station> stations) {
        fuelStations.addAll(stations);
    }

    public void clear() {
        fuelStations.clear();
    }

    public void resetStations(List<Station> stations) {
        clear();
        addStations(stations);
    }

    public final List<Station> getStationList() {
        return fuelStations;
    }

    public Station getStation(int position) {
        if (position < fuelStations.size()) {
            return fuelStations.get(position);
        }

        return null;
    }
}
